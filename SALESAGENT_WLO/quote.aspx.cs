﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace SALESAGENT_WLO
{
    public partial class quote : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if(Request.QueryString["QuotID"]!=null)
                {
                    bindQuote(Request.QueryString["QuotID"].ToString());
                    bindQuoteMain(Request.QueryString["QuotID"].ToString());
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }

            }
        }

        private void bindQuoteMain(string quotationID)
        {
            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {

                string query = "SELECT * FROM QUOTATION WHERE QT_ID_PK = '"+quotationID+"'";
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ltCompany.Text = dr["COMPANY"].ToString() == "1" ? "APHEALTH" : "AVLI BIOCARE";
                            ltAgent.Text = dr["AGENT"].ToString();
                            ltDate.Text = dr["DATE"].ToString();
                            ltCode.Text = dr["QTCODE"].ToString();
                            ltCustomerCode.Text = dr["CUSTOMERCODE"].ToString();
                            ltCustomerName.Text = dr["CUSTOMERNAME"].ToString();
                            hiddenqtcode.Value = dr["QTCODE"].ToString();

                        }
                    }

                    con.Close();

                }

            }
        }
        private void bindQuote(string quotationID)
        {
           
            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {

                string query = "SELECT STOCKCODE,STOCKNAME,QTY,UOM,PRICE FROM QUOTATION_DETAIL WHERE QUOTATION_FK = '" + quotationID + "'";
                using (SqlDataAdapter da = new SqlDataAdapter(query, con))
                {
                    using (DataTable dt = new DataTable())
                    {
                        da.Fill(dt);
                        int no = dt.Rows.Count;
                        if (no > 0)
                        {

                            rptQuote.DataSource = dt;
                            rptQuote.DataBind();
                            Double totalMarks = dt.Select().Sum(p => Convert.ToDouble(p["PRICE"]) * Convert.ToDouble(p["QTY"]));
                            (rptQuote.Controls[rptQuote.Controls.Count - 1].Controls[0].FindControl("lblTotal") as Label).Text = "Total Amount: " + totalMarks.ToString();
                        }
                        else
                        {

                        }
                    }

                }

            }

        }

    
    }
}