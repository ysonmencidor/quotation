﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SALESAGENT_WLO.ADMIN
{
    public partial class primary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void btnOut_Click(object sender, EventArgs e)
        {
            Session.Remove("ADMIN");
            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
        }
    }
}