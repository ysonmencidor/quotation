﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;


namespace SALESAGENT_WLO.ADMIN
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if(Session["ADMIN"] != null)
                {
                    Response.Redirect("primary.aspx");
                }
            }
        }

        protected void btnlogin_Click(object sender, EventArgs e)
        {
            string use = ConfigurationManager.AppSettings["U"];
            string pass = ConfigurationManager.AppSettings["P"];
            if (txtusername.Text == use)
            {
                if (txtpassword.Text == pass)
                {
                    Session["ADMIN"] = "ADMIN2020";
                    Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
                }
                else
                {

                    divalert.Visible = true;
                }
            }
            else
            {
                 divalert.Visible = true;
            }
        }
    }
}