﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace SALESAGENT_WLO
{
    public partial class Login : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["userName"] != null && Session["company_code"] != null && Session["agent_id"] != null)
                {
                    Response.Redirect("Index.aspx");
                }
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                SqlCommand cmd = new SqlCommand("select PASSWORD,COMPANY,ACCOUNT_ID,DEACTIVATED from account where USERNAME = @use", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@use", txtUsername.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {

                    if (dt.Rows[0][0].ToString() == txtPassword.Text)
                    {

                        if(dt.Rows[0][3].ToString() == "True")
                        {
                            txtPassword.Text = "";
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Account Deactivated .');", true);

                        }
                        else
                        {
                            Session["userName"] = txtUsername.Text;
                            Session["company_code"] = dt.Rows[0][1].ToString();
                            Session["agent_id"] = dt.Rows[0][2].ToString();
                            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
                        }
                           
                       }
                    else
                    {   

                        txtPassword.Focus();
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Invalid password.');", true);

                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Cannot find user account.');", true);

                }
            }

            }
    }
}