﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using Quartz;
using System.Net.Mail;
using System.IO;

namespace SALESAGENT_WLO
{
    public class QTMODIFYNOTIFTOWLO : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {

                SqlCommand cmd = new SqlCommand("SPRemindWLOMODIFIEDQUOTATION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                int no = dt.Rows.Count;
                if (no > 0)
                {
                    string QT_DETAILS_SEND_TO_WLO = "SELECT COMPANY,AGENT,CUSTOMERNAME,QTCODE,LAST_MODIFIED FROM QUOTATION WHERE QT_ID_PK = @QUOT_FK";
                    SqlCommand cmDEtails = new SqlCommand(QT_DETAILS_SEND_TO_WLO, con);
                    cmDEtails.Parameters.AddWithValue("@QUOT_FK", dt.Rows[0][0].ToString());
                    SqlDataAdapter daDetails = new SqlDataAdapter(cmDEtails);
                    DataTable dtDetails = new DataTable();
                    daDetails.Fill(dtDetails);
                    int noDetails = dtDetails.Rows.Count;
                    if (noDetails > 0)
                    {
                        //string builder html head and body
                        #region 
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<html><head>");
                        sb.Append("<style>" +
                      " .border-0 { border: 0 !important; }  " +
                "table {" +
                "    background-color: transparent;" +
                "}  caption {" +
                "    padding-top: 8px;" +
                "    padding-bottom: 8px;" +
                "    color: #777;" +
                "    text-align: left;" +
                "} th {" +
                "    text-align: left;" +
                "} .table {" +
                "    width: 100%;" +
                "    margin-bottom: 20px;" +
                "}" +
                " .table > thead > tr > th," +
                "    .table > tbody > tr > th," +
                "    .table > tfoot > tr > th," +
                "    .table > thead > tr > td," +
                "    .table > tbody > tr > td," +
                "    .table > tfoot > tr > td {" +
                "        padding: 8px;" +
                "        line-height: 1.42857143;" +
                "        vertical-align: top;" +
                "        border-top: 1px solid #ddd;" +
                "    } .table > thead > tr > th {" +
                "        vertical-align: bottom;" +
                "        border-bottom: 2px solid #ddd;" +
                "    }   .table > caption + thead > tr:first-child > th," +
                "    .table > colgroup + thead > tr:first-child > th," +
                "    .table > thead:first-child > tr:first-child > th," +
                "    .table > caption + thead > tr:first-child > td," +
                "    .table > colgroup + thead > tr:first-child > td," +
                "    .table > thead:first-child > tr:first-child > td {" +
                "        border-top: 0;" +
                "    }  .table > tbody + tbody {" +
                "        border-top: 2px solid #ddd;" +
                "    }  .table .table {" +
                "        background-color: #fff;" +
                "    } .table-condensed > thead > tr > th," +
                ".table-condensed > tbody > tr > th," +
                ".table-condensed > tfoot > tr > th," +
                ".table-condensed > thead > tr > td," +
                ".table-condensed > tbody > tr > td," +
                ".table-condensed > tfoot > tr > td {" +
                "   padding: 5px;" +
                "} .table-bordered {" +
                "    border: 1px solid #ddd;" +
                "} .table-bordered > thead > tr > th," +
                "    .table-bordered > tbody > tr > th," +
                "    .table-bordered > tfoot > tr > th," +
                "    .table-bordered > thead > tr > td," +
                "    .table-bordered > tbody > tr > td," +
                "    .table-bordered > tfoot > tr > td {" +
                "        border: 1px solid #ddd;" +
                "    } table-bordered > thead > tr > th," +
                "    .table-bordered > thead > tr > td {" +
                "        border-bottom-width: 2px;" +
                "    } .table-striped > tbody > tr:nth-of-type(odd) {" +
                "    background-color: #f9f9f9;" +
                "} .table-hover > tbody > tr:hover {" +
                "    background-color: #f5f5f5;" +
                "} table col[class*='col-'] {" +
                "    position: static;" +
                "    display: table-column;" +
                "    float: none;" +
                "} table td[class*='col-']," +
                "table th[class*='col-'] {" +
                "    position: static;" +
                "    display: table-cell;" +
                "    float: none;" +
                "} .table > thead > tr > td.active," +
                ".table > tbody > tr > td.active," +
                ".table > tfoot > tr > td.active," +
                ".table > thead > tr > th.active," +
                ".table > tbody > tr > th.active," +
                ".table > tfoot > tr > th.active," +
                ".table > thead > tr.active > td," +
                ".table > tbody > tr.active > td," +
                ".table > tfoot > tr.active > td," +
                ".table > thead > tr.active > th," +
                ".table > tbody > tr.active > th," +
                ".table > tfoot > tr.active > th {" +
                "    background-color: #f5f5f5;" +
                "} .table-hover > tbody > tr > td.active:hover," +
                ".table-hover > tbody > tr > th.active:hover," +
                ".table-hover > tbody > tr.active:hover > td," +
                ".table-hover > tbody > tr:hover > .active," +
                ".table-hover > tbody > tr.active:hover > th {" +
                "    background-color: #e8e8e8;" +
                "} .table > thead > tr > td.success," +
                ".table > tbody > tr > td.success," +
                ".table > tfoot > tr > td.success," +
                ".table > thead > tr > th.success," +
                ".table > tbody > tr > th.success," +
                ".table > tfoot > tr > th.success," +
                ".table > thead > tr.success > td," +
                ".table > tbody > tr.success > td," +
                ".table > tfoot > tr.success > td," +
                ".table > thead > tr.success > th," +
                ".table > tbody > tr.success > th," +
                ".table > tfoot > tr.success > th {" +
                "    background-color: #dff0d8;" +
                "} .table-hover > tbody > tr > td.success:hover," +
                ".table-hover > tbody > tr > th.success:hover," +
                ".table-hover > tbody > tr.success:hover > td," +
                ".table-hover > tbody > tr:hover > .success," +
                ".table-hover > tbody > tr.success:hover > th {" +
                "    background-color: #d0e9c6;" +
                "} .table > thead > tr > td.info," +
                ".table > tbody > tr > td.info," +
                ".table > tfoot > tr > td.info," +
                ".table > thead > tr > th.info," +
                ".table > tbody > tr > th.info," +
                ".table > tfoot > tr > th.info," +
                ".table > thead > tr.info > td," +
                ".table > tbody > tr.info > td," +
                ".table > tfoot > tr.info > td," +
                ".table > thead > tr.info > th," +
                ".table > tbody > tr.info > th," +
                ".table > tfoot > tr.info > th {" +
                "    background-color: #d9edf7;" +
                "} .table-hover > tbody > tr > td.info:hover," +
                ".table-hover > tbody > tr > th.info:hover," +
                ".table-hover > tbody > tr.info:hover > td," +
                ".table-hover > tbody > tr:hover > .info," +
                ".table-hover > tbody > tr.info:hover > th {" +
                "    background-color: #c4e3f3;" +
                "} .table > thead > tr > td.warning," +
                ".table > tbody > tr > td.warning," +
                ".table > tfoot > tr > td.warning," +
                ".table > thead > tr > th.warning," +
                ".table > tbody > tr > th.warning," +
                ".table > tfoot > tr > th.warning," +
                ".table > thead > tr.warning > td," +
                ".table > tbody > tr.warning > td," +
                ".table > tfoot > tr.warning > td," +
                ".table > thead > tr.warning > th," +
                ".table > tbody > tr.warning > th," +
                ".table > tfoot > tr.warning > th {" +
                 "   background-color: #fcf8e3;" +
                "} .table-hover > tbody > tr > td.warning:hover," +
                ".table-hover > tbody > tr > th.warning:hover," +
                ".table-hover > tbody > tr.warning:hover > td," +
                ".table-hover > tbody > tr:hover > .warning," +
                ".table-hover > tbody > tr.warning:hover > th {" +
                 "   background-color: #faf2cc;" +
                "} .table > thead > tr > td.danger," +
                ".table > tbody > tr > td.danger," +
                ".table > tfoot > tr > td.danger," +
                ".table > thead > tr > th.danger," +
                ".table > tbody > tr > th.danger," +
                ".table > tfoot > tr > th.danger," +
                ".table > thead > tr.danger > td," +
                ".table > tbody > tr.danger > td," +
                ".table > tfoot > tr.danger > td," +
                ".table > thead > tr.danger > th," +
                ".table > tbody > tr.danger > th," +
                ".table > tfoot > tr.danger > th {" +
                "    background-color: #f2dede;" +
                "} .table-hover > tbody > tr > td.danger:hover," +
                ".table-hover > tbody > tr > th.danger:hover," +
                ".table-hover > tbody > tr.danger:hover > td," +
                ".table-hover > tbody > tr:hover > .danger," +
                ".table-hover > tbody > tr.danger:hover > th {" +
                 "   background-color: #ebcccc;" +
                "}.table-responsive {" +
                  "  min-height: .01%;" +
                 "   overflow-x: auto;" +
                "} table {" +
                  "  border-spacing: 0;" +
                 "   border-collapse: collapse;" +
                "}  td," +
                "th {padding: 0;}" +
             "</style>");
                        sb.Append("</head>");
                        sb.Append("<body>");
                        #endregion

                        string companyName = dtDetails.Rows[0][0].ToString() == "1" ? "APTHEALTH" : "AVLI BIOCARE";
                        var dateCanceled = Convert.ToDateTime(dtDetails.Rows[0][4].ToString()).ToString("yyyy-MM-dd hh:mm tt");
                        string bodys = "";
                        bodys += "<table class='table table-responsive table-striped'>";
                        bodys += "<tr><th class='border-0' colspan='5'>COMPANY : " + companyName + "</th></tr>";
                        bodys += "<tr><th class='border-0' colspan='5'>AGENT : " + dtDetails.Rows[0][1].ToString() + "</th></tr>";
                        bodys += "<tr><th class='border-0' colspan='5'>CUSTOMER : " + dtDetails.Rows[0][2].ToString() + "</th></tr>";
                        bodys += "<tr><th class='border-0' colspan='5'>QUOTATION CODE : " + dtDetails.Rows[0][3].ToString() + "</th></tr>";
                        bodys += "<tr><th class='border-0' colspan='5'>MODIFY DATE: " + dateCanceled + "</th></tr>";
                        bodys += "<tbody>";
                        bodys += "</tbody></table>";

                        string htmlbody = "" +
                       "<br />" +
                       "<div style='border-top:3px solid #22BCE5'>&nbsp;</div>" +
                       "<span style='font-family:Arial;font-size:10pt'>" +
                       "NOTICE FOR AMENDMENT OF QUOTATION," +
                       "<br />" +
                       "<br />" +
                       "<br />" +
                        bodys +
                       "<br />" +
                       "<br />" +
                       "<br />" +
                       "This email is auto-generated. DO NOT REPLY." +
                       "<br />" +
                       "Nutratech Biopharma Inc." +
                        "<div style='border-top:3px solid #22BCE5'>&nbsp;</div>";
                        sb.Append(htmlbody);
                        sb.Append("</body>");
                        sb.Append("</html>");

                        //string body = this.PopulateBody(bodys);
                        string LISEMAIL = GetEmails(dtDetails.Rows[0][1].ToString()).Substring(0, GetEmails(dtDetails.Rows[0][1].ToString()).Length - 1);
                        SendHtmlFormattedEmail(LISEMAIL, "AMENDMENT OF QUOATION", sb.ToString());
                        // System.Console.WriteLine(dt.Rows[0].ToString());

                    }

                }


            }

        }

        private string GetEmails(string username)
        {
            string EMAIL_RESULT = "";
            string q = @"SELECT C.EMAIL AS WLO_EMAIL FROM ASSIGNMENT AS A                     
                            LEFT JOIN WLO_ACCOUNT AS C
                            ON A.WLO_ID_FK = C.ID
                            LEFT JOIN account AS D
                            on A.AGENT_ID_FK = D.ACCOUNT_ID
                            WHERE D.USERNAME = '" + username + "'";

            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmdGetEmail = new SqlCommand(q, con))
                {

                    if (con.State == ConnectionState.Closed)

                    {
                        con.Open();
                    }
                    SqlDataReader dr = cmdGetEmail.ExecuteReader();
                    while (dr.Read())
                    {

                        EMAIL_RESULT += dr["WLO_EMAIL"].ToString() + ",";

                    }
                    con.Close();

                }
            }
            return EMAIL_RESULT;
        }

        private void SendHtmlFormattedEmail(string recepientEmail, string subject, string body)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(recepientEmail);
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
                NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                smtp.Send(mailMessage);
            }
        }
    }
}