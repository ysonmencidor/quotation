﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SALESAGENT_WLO
{
    public class QNEConnectionString
    {
        public static string ChooseConnection(string company_code)
        {
            string IC_APTHEALTH_CODE = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["IC_APTHEALTH_CODE"]);
            string IC_BIOCARE_CODE = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["IC_BIOCARE_CODE"]);

            if (company_code == IC_APTHEALTH_CODE)
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["QNE_APHI"].ConnectionString;
            }
            else if (company_code == IC_BIOCARE_CODE)
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["QNE_AVLI"].ConnectionString;
            }
            else
            {
                throw new System.InvalidOperationException("No qne connection string specified for company_code: " + company_code);
            }

        }

     }

    public class SQLCONNECTION
    {
         public static string SQLSVRConnection()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        }
    }
}