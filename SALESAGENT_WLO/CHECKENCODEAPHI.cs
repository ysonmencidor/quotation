﻿using System.Data;
using System.Data.SqlClient;
using FirebirdSql.Data.FirebirdClient;
using Quartz;


namespace SALESAGENT_WLO
{
    public class CHECKENCODEAPHI : IJob
    {
        public void Execute(IJobExecutionContext context)
        {

            using (SqlConnection sqlcon = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {

                using (SqlCommand cmd = new SqlCommand("SELECT * FROM QUOTATION WHERE COMPANY = 1 AND CANCELED = 0 AND QNE = 0", sqlcon))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        int no = dt.Rows.Count;
                        if(no>0)
                        {
                            foreach(DataRow row in dt.Rows)
                            {

                                using (FbConnection fbcon = new FbConnection(QNEConnectionString.ChooseConnection("1")))
                                {
                                    using (FbCommand FbCmd = new FbCommand("SELECT QUOTATIONCODE,FIRSTCREATEDDATE,FIRSTCREATEDUSERCODE FROM QUOTATION WHERE QUOTATIONREFNO = '"+ row["QTCODE"].ToString() + "'", fbcon))
                                    {
                                        using (FbDataAdapter fbda = new FbDataAdapter(FbCmd))
                                        {
                                            DataTable dt2 = new DataTable();
                                            fbda.Fill(dt2);
                                            int n2 = dt2.Rows.Count;
                                            if (n2 > 0)
                                            {
                                                using (SqlCommand cmdUpdateQuot = new SqlCommand("UPDATE QUOTATION SET QNE = 1,QNE_DATE = @qneDate,QNE_QTCODE = @qneqtCode where QTCODE = @qtCode", sqlcon))
                                                {
                                                    cmdUpdateQuot.CommandType = CommandType.Text;
                                                    cmdUpdateQuot.Parameters.AddWithValue("@qneDate", dt2.Rows[0][1].ToString());
                                                    cmdUpdateQuot.Parameters.AddWithValue("@qneqtCode", dt2.Rows[0][0].ToString());
                                                    cmdUpdateQuot.Parameters.AddWithValue("@qtCode", row["QTCODE"].ToString());

                                                    if (sqlcon.State == ConnectionState.Closed)
                                                    {
                                                        sqlcon.Open();
                                                    }
                                                    cmdUpdateQuot.ExecuteNonQuery();
                                                    sqlcon.Close();
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }

                    }
                }


            

            }

       
        }

   }
}