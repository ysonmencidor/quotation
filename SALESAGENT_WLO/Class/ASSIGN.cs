﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SALESAGENT_WLO
{
    public class ASSIGN
    {
        public int ASISGNED_ID { get; set; }
        public int DEPARTMENT_ID_FK { get; set; }
        public int AGENT_ID_FK { get; set; }
        public int WLO_ID_FK { get; set; }
        public string DEPARTMENT_NAME { get; set; }
        public string DEPARTMENT_DESCRIPTION { get; set; }
        public string COMPANY { get; set; }
        public string AGENT_NAME { get; set; }
        public string AGENT_EMAIL { get; set; }
        public string WLO_NAME { get; set; }
        public string WLO_EMAIL { get; set; }

    }
}