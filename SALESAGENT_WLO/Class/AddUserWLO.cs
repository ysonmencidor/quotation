﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SALESAGENT_WLO
{
    public class AddUserWLO
    {
        public string USERNAME { get; set; }
        public string NAME { get; set; }
        public string EMAIL { get; set; }
    }
}