﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SALESAGENT_WLO
{
    public class QT
    {

        public string QTCODE { get; set; }
        public string CUSTOMER { get; set; }
        public string AGENT { get; set; }
        public DateTime DATE_SAVE { get; set; }
        public string CUSTOMERNAME { get; set; }
        public string AGENT_ID { get; set; }
        public int COMPANY { get; set; }

    }
}