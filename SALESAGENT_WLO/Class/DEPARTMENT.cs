﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SALESAGENT_WLO
{
    public class DEPARTMENT
    {
        public string DEPARTMENT_NAME { get; set; }
        public string DEPARTMENT_DESCRIPTION { get; set; }
    }
}