﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SALESAGENT_WLO
{
    public class AddUser
    {
        public string USERNAME { get; set; }
        public string CONTACTNUMBER { get; set; }
        public string COMPANY { get; set; }
        public string EMAIL { get; set; }
        public string NAME { get; set; }
        public int ID { get; set; }
    }
}