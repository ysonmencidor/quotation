﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SALESAGENT_WLO
{
    public class QUOTATION
    {
        public string STOCKCODE { get; set; }
        public string STOCKNAME { get; set; }
        public string QTY { get; set; }
        public string UOM { get; set; }
        public string PRICE { get; set; }

    }
}