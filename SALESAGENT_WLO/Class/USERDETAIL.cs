﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SALESAGENT_WLO
{
    public class USERDETAIL
    {
        public int ID { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public string EMAIL { get; set; }
        public string NAME { get; set; }
        public bool DEACTIVATED { get; set; }

        public string COMPANY { get; set; }
        public string NUMBER { get; set; }

    }
}