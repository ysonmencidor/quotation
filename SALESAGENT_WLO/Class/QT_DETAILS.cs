﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SALESAGENT_WLO
{
    public class QT_DETAILS
    {
        public string STOCKCODE { get; set; }
        public string STOCKNAME { get; set; }
        public double QTY { get; set; }
        public string UOM { get; set; }
        public double PRICE { get; set; }
        public double AMOUNT { get; set; }


    }
}