﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SALESAGENT_WLO
{
    public class QT2
    {
        public string QT_ID { get; set; }
        public string QTCODE { get; set; }
        public string CUSTOMER { get; set; }
        public string AGENT { get; set; }
        
        public string CUSTOMERNAME { get; set; }
        public DateTime DATE_SAVE
        {
            get; set;
        }
    }
}