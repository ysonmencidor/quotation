﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SALESAGENT_WLO
{
    public class LASTPURCHASE
    {
        public string CUSTOMERCODE { get; set; }

        public string SALESAGENT { get; set; }

        public string STOCKCODE { get; set; }

        public double PRICE { get; set; }
    }
}