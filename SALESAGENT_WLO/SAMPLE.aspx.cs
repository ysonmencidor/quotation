﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
using System.Configuration;

namespace SALESAGENT_WLO
{
    public partial class SAMPLE : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string EMAIL_RESULT = "";
                string q = @"SELECT C.EMAIL AS WLO_EMAIL FROM ASSIGNMENT AS A                     
                            LEFT JOIN WLO_ACCOUNT AS C
                            ON A.WLO_ID_FK = C.ID
                            LEFT JOIN account AS D
                            on A.AGENT_ID_FK = D.ACCOUNT_ID
                            WHERE D.USERNAME = 'jaysonrm'";
                using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
                {
                    using (SqlCommand cmdGetEmail = new SqlCommand(q, con))
                    {

                        if (con.State == ConnectionState.Closed)

                        {
                            con.Open();
                        }
                        SqlDataReader dr = cmdGetEmail.ExecuteReader();
                        while (dr.Read())
                        {
                        
                          EMAIL_RESULT += dr["WLO_EMAIL"].ToString() + ",";
                     
                       }
                        con.Close();
                    }
                }
                string LISEMAIL = EMAIL_RESULT.Substring(0, EMAIL_RESULT.Length - 1);
                Response.Write(LISEMAIL);
            }
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {

                SqlCommand cmd = new SqlCommand("SPRemindWLONEWINSERTEDQUOTATION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                int no = dt.Rows.Count;
                if (no > 0)
                {
                    string QT_DETAILS_SEND_TO_WLO = "SELECT * FROM QUOTATION_DETAIL WHERE QUOTATION_FK = @QUOT_FK";
                    SqlCommand cmDEtails = new SqlCommand(QT_DETAILS_SEND_TO_WLO, con);
                    cmDEtails.Parameters.AddWithValue("@QUOT_FK", dt.Rows[0][0].ToString());
                    SqlDataAdapter daDetails = new SqlDataAdapter(cmDEtails);
                    DataTable dtDetails = new DataTable();
                    daDetails.Fill(dtDetails);
                    int noDetails = dtDetails.Rows.Count;
                    if (noDetails > 0)
                    {

                        string bodys = "";
                        bodys += "</html><body>";
                        bodys = "<table class='table table-responsive table-striped'>";
                        bodys += "<tr><th  colspan='5'>" + dt.Rows[0][0].ToString() + "</th></tr>";
                        bodys += "<tr><th>CODE</th><th>STOCKNAME</th><th>QTY</th><th>UOM</th><th>PRICE</th></tr>";

                        foreach (DataRow dtRowDetails in dtDetails.Rows)
                        {
                            bodys += "<tr>";
                            bodys += "<td>" + dtRowDetails["STOCKCODE"].ToString() + "</td>";
                            bodys += "<td>" + dtRowDetails["STOCKNAME"].ToString() + "</td>";
                            bodys += "<td>" + dtRowDetails["QTY"].ToString() + "</td>";
                            bodys += "<td>" + dtRowDetails["UOM"].ToString() + "</td>";
                            bodys += "<td>" + dtRowDetails["PRICE"].ToString() + "</td>";
                            bodys += "</tr>";
                        }
                        bodys += "</table></body></html>";

                        SendHtmlFormattedEmail("ysonmencidorj@gmail.com", dt.Rows[0][1].ToString(), bodys);
                       // System.Console.WriteLine(dt.Rows[0].ToString());

                    }

                }
            }
            }
        private void SendHtmlFormattedEmail(string recepientEmail, string subject, string body)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
                NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                smtp.Send(mailMessage);
            }
        }
    }
}