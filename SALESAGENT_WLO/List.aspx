﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="SALESAGENT_WLO.List" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>- LIST -</title>   
     <link href="img/ntbi-icon.png" rel="shortcut icon" type="image/x-icon" />
     <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-grid.min.css" rel="stylesheet" />
    <link href="Content/font-awesome.min.css" rel="stylesheet" /> 
    <script src="Scripts/jquery-3.4.1.min.js"></script>
    <link href="Content/DataTables/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="Content/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery-ui.min.css" rel="stylesheet" />

    <script src="Scripts/DataTables/jquery.dataTables.min.js"></script>

    <script src="Scripts/DataTables/dataTables.buttons.min.js"></script>
    <script src="Scripts/DataTables/buttons.flash.min.js"></script>
    <link href="Content/jquery-confirm.css" rel="stylesheet" />
    <script src="Scripts/jquery-confirm.js"></script>
    <link href="Content/DataTables/css/buttons.dataTables.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui-1.12.1.min.js"></script>
  <%--  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet"/>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>--%>
    
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="js/List.js"></script>

    <link href="Content/Style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
       <div class="container-fluid">
            <div class="row">
                <div class="col-md-10 offset-md-1 col-12">

                        <div class="x_panel shadow" style="margin-top:5px">
               <div class="x_title">

                             <div class="row">
                      <div class="col-md-6 offset-md-3 col-10 offset-1">
                     <img id="IMG_LOGO" class="img-fluid" style="margin:auto;cursor:pointer"/>
                         </div>
                 </div>
                    

                </div>
                <div class="x_content">
                                         <div class="row">

      <div class="col-12">


<ul style="list-style:none" class="stepNav threeWide">
  <li><a href="Index.aspx" ><i class="fa fa-dashboard"> QUOTATION</i></a></li>
    <li class="selected"><a href="List.aspx"><i class="fa fa-list-alt"> LIST</i></a></li>
    <li><a href="Settings.aspx"><i class="fa fa-gear"> Settings</i></a></li>
    <li><asp:LinkButton runat="server" ID="btnLogOut" OnClick="btnLogOut_Click"><i class="fa fa-sign-out"> Log out</i></asp:LinkButton></li>
</ul>
          </div>
                                         </div>    
                      <div class="x_panel shadow">
                                                                     <div class="row text-center">
                                      <div class="col-4 bg-success text-white">ENCODED</div>
                                      <div class="col-4 bg-light" style="border:1px solid #dedede">PENDING</div>
                                      <div class="col-4 bg-danger text-white">CANCELED</div>
                                  </div>
                                         <div class="row">
                              <div class="col-12 col-lg-12 col-md-12">
                            
                                  <br />

                                  <div class="row">
                                      <div class="col-12 col-lg-6 col-md-6">
      <div class="custom-control custom-checkbox custom-control-inline">
    <input type="checkbox" class="custom-control-input" id="hide2"/>
    <label class="custom-control-label" for="hide2">HIDE ENCODED </label>
</div>
                                  <div class="custom-control custom-checkbox custom-control-inline">
    <input type="checkbox" class="custom-control-input" id="hide1"/>
    <label class="custom-control-label" for="hide1">HIDE CANCELED</label>
</div>                                
   
                                          </div>
                                
                                      <div class="col-12 col-lg-6 col-md-6">
                                      

                                            <div class="form-group form-inline">
    <span id="date-label-from" class="date-label">FROM: </span><input class="date_range_filter date" type="text" id="datepicker_from" />
    <span id="date-label-to" class="date-label">TO:</span><input class="date_range_filter date" type="text" id="datepicker_to" />
                                </div>
                                      </div>
                                  </div>
                               
                                  <br />
                                 <table id="QuotTable" class="table table-striped table-bordered table-responsive-sm nowrap" style="width:100%">

                                     <thead>
                                         <tr>
                                             <th style="width:5%"></th>
                                             <th style="display:none"></th>
                                             <th style="width:15%">QTCODE</th>
                                             <th style="width:15%">CREATED</th>
                                             <th style="width:15%">QNE CODE</th>
                                             <th style="width:15%">QNE DATE</th>
                                             <th style="width:30%">CUSTOMER</th>
                                             <th style="width:5%"></th>
                                         </tr>
                                     </thead>
                      
                                         </table>
                            </div>
         
                   </div>
         
                    <!-- dito -->

                    </div>
                </div>
            </div>
                 </div>
          </div>
         </div>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-scrollable modal-lg mt-0" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">EDIT QUOTATION : <label class="text-info" id="lblQuot"></label> </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                      <div class="container-fluid">
                          <div class="row">
                              <div class="col-12 col-md-6 col-lg-6">
                              <input type="hidden" id="editID"/>    
            <table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
             <tr>
             <td>Customer : </td>
             <td><label class="text-secondary form-check-label  " id="lblName"></label></td>
             </tr>
             <tr>
             <td>Date Created : </td>
             <td><label class="text-secondary form-check-label  " id="lblDateCreate"></label></td>
             </tr>
        </table>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-12 col-md-12 col-lg-12">
                      <table class="table table-sm table-bordered css-serial" id="tbDetails"  style="width:100%;padding:8px">
                    <thead class="thead-dark text-center">
                        <tr>
                                            <th style="width:1%">#</th>
                                            <th style="width:65%">STOCKNAME</th>
                                            <th style="width:10%">QTY</th>
                                            <th style="width:15%">U.PRICE</th>
                                            <th style="width:7%">UOM</th>
                                            <th style="width:3%"></th>
                    </tr></thead>
                          <tbody></tbody>
                    <tfoot><tr></tr></tfoot>
                </table>
                              <button type="button" id="btnAdd" class="btn btn-sm btn-primary classAdd"><span class="fa fa-plus-square"> new row</span></button>  
                            
                              </div>
                          </div>
                      </div>


                  </div>
                  <div class="modal-footer">
                                       
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
                   <button type="button" id="btnSubmit" class="btn btn-primary btn-sm SAVEQUOTATION"><span class="fa fa-save"> Save Quotation</span> </button>  
            
                  </div>
                </div>
              </div>
            </div>
          <input id="hidden_company" type="hidden" runat="server" />
          <input id="hidden_user" type="hidden" runat="server" />

        <div id="myAlert"></div>
    </form>
</body>
</html>
