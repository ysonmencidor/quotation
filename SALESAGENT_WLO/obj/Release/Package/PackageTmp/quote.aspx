﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="quote.aspx.cs" Inherits="SALESAGENT_WLO.quote" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Quotation</title>    
    <link rel="icon" type="image/x-icon" href="../img/ntbi-icon.png" />
         <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/Style.css" rel="stylesheet" />
    <script type="text/javascript">
        function copy() {
            var copyText = document.getElementById("hiddenqtcode");
            copyText.select();
            document.execCommand("copy");
            alert("Copied the text: " + copyText.value);
        }
        function myFunction() {
            window.print();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
   <asp:HiddenField runat="server" id="hiddenqtcode"/>
         <div class="container">
    <div class="row">
      
        <div class="col-12">

            <asp:Literal runat="server" ID="literalStatus"></asp:Literal>
    
        <div class="mt-3">
            <table class="table table-bordered">
                <tr>
                    <td>COMPANY</td>
                    <td><asp:Literal runat="server" ID="ltCompany"></asp:Literal></td>
                    <td>CODE</td>
                    <td style="cursor:pointer" onclick="copy()"><asp:Literal runat="server" ID="ltCode"></asp:Literal></td>
                </tr>
                <tr>
                    <td>DATE</td>
                    <td><asp:Literal runat="server" ID="ltDate"></asp:Literal></td> 
                    <td>Customer</td>
                    <td><asp:Literal runat="server" ID="ltCustomerCode"></asp:Literal> </td>
                </tr>
                <tr>
                    <td>AGENT</td>
                    <td><asp:Literal runat="server" ID="ltAgent"></asp:Literal></td>
                    <td class="text-center" colspan="2"><asp:Literal runat="server" ID="ltCustomerName"></asp:Literal></td>
               </tr>
           
            </table>

            <table class="table table-hover table-bordered css-serial">
               <thead>
                   <tr>
                       <th>#</th>
                       <th>STOCKCODE</th>
                       <th>STOCKNAME</th>
                       <th>QTY</th>
                       <th>UOM</th>
                       <th>PRICE</th>
                       <th>AMOUNT</th>
                   </tr>
               </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="rptQuote">
                        <ItemTemplate>
                            <tr>
                                <td></td>
                                <td><%# Eval("STOCKCODE") %></td>
                                <td><%# Eval("STOCKNAME") %></td>
                                <td><%# Eval("QTY") %></td>
                                <td><%# Eval("UOM") %></td>
                                <td><%# Eval("PRICE") %></td>
                                <td style="max-width:5%"><%# Convert.ToDouble(Eval("PRICE")) * Convert.ToDouble(Eval("QTY")) %></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            <tr>
                                <th colspan="6"></th>
                                <th><asp:Label runat="server" ID="lblTotal"/></th>
                            </tr>
                        </FooterTemplate>
                    </asp:Repeater>
                </tbody>
            </table>


    
        </div>
                   <button class="d-print-none btn btn-info" onclick="myFunction()">Print this page </button>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
