﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="SALESAGENT_WLO.Settings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
       <title> - Settings - </title>
     <link href="img/ntbi-icon.png" rel="shortcut icon" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-grid.min.css" rel="stylesheet" />
    <link href="Content/Style.css" rel="stylesheet" />
    <link href="Content/font-awesome.min.css" rel="stylesheet" /> 
    <script src="Scripts/jquery-3.4.1.min.js"></script>  
    <link href="Content/DataTables/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="Content/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/DataTables/jquery.dataTables.min.js"></script>
    <link href="Content/jquery-confirm.css" rel="stylesheet" />
    <script src="Scripts/jquery-confirm.js"></script>
    <script src="js/AGENTSETTINGS.js"></script>
    <link href="Content/Style.css" rel="stylesheet" />
    
  

</head>
<body>
    <form id="form1" runat="server">
    
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10 offset-md-1 col-12">

                        <div class="x_panel shadow" style="margin-top:5px">
               <div class="x_title">

                             <div class="row">
                      <div class="col-md-6 offset-md-3 col-10 offset-1">
                     <img id="IMG_LOGO" class="img-fluid" style="margin:auto;"/>
                         </div>
                 </div>
                    

                </div>
                <div class="x_content minVH">
                                         <div class="row">

      <div class="col-12">


<ul style="list-style:none" class="stepNav threeWide">
  <li><a href="Index.aspx"><i class="fa fa-dashboard"> QUOTATION</i></a></li>
    <li><a href="List.aspx"><i class="fa fa-list-alt"> LIST</i></a></li>
    <li class="selected"><a href="Settings.aspx"><i class="fa fa-gear"> Settings</i></a></li>
    <li><asp:LinkButton runat="server" ID="btnLogOut" OnClick="btnLogOut_Click"><i class="fa fa-sign-out"> Log out</i></asp:LinkButton></li>
</ul>
          </div>
                                         </div>    
                      <div class="x_panel shadow">
                
           
                          <div class="row">
                              <div class="col-12 col-lg-12 col-md-12">

<div class="card">
  <div class="card-header">
    <ul class="nav nav-tabs card-header-tabs">
 <%--     <li class="nav-item">
        <a class="nav-link active" id="v-pills-tab1" data-toggle="pill" href="#tab1" role="tablist" aria-controls="v-pills-tab1" aria-selected="true"><i class="fa fa-warning"> Notification</i> </a>
   </li>--%>
      <li class="nav-item">
        <a class="nav-link active" href="#tab2" id="v-pills-tab2" data-toggle="pill" role="tablist" aria-controls="v-pills-tab2"><i class="fa fa-user-md"> Account</i> </a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Report</a>
      </li>
    </ul>
  </div>
  <div class="card-body">
      <div class="tab-content">
          <%--<%--<%-- <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="v-pills-tab1">

                           <div class="card">
              <div class="card-body">
               <div class="row">
                  <div class="col-12 col-sm-12 col-lg-7 ">
                <h6 class="card-title">ASSIGNED WLO</h6>
                  
            
                      <table id="tblAssignedWLO"  class="table table-striped table-bordered table-responsive-sm nowrap " style="width:100%">
                              <thead>
                                  <tr>
                                      <th>DEPARTMENT</th>
                                      <th>EMAIL</th>
                                      <th style="display:none"></th>
                                      <th></th>
                                  </tr>
                              </thead>
                      </table>
                      <br />
              <%--        <div class="text-right">
  <button type="submit" id="btnRemover" class="btn btn-primary btn-sm ">REMOVE <span class="fa fa-chevron-right"></span></button>
                          </div>--%>
       <%--               </div>

                  <div class="col-12 col-sm-12 col-lg-5">
                      
                <h6 class="card-title">UNASSIGNED WLO</h6>

                  
    <select class="form-control" id="ddWLOUNAssigned">
    </select>
                      <br />
                      <div class="text-left">
  <button type="submit" id="btnAddWLO" class="btn btn-primary btn-sm"><span class="fa fa-chevron-left"></span> ADD</button>
                          </div>
                  </div>

              </div>
                  </div>
            </div>--%>
                  <%--</div> <!-- end of tab1 settings -->--%>
   
                <div class="tab-pane fade show active " id="tab2" role="tabpanel" aria-labelledby="v-pills-tab2">
                <div role="form" id="CreateForm">
                         <div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" required="required" disabled="disabled" id="name" placeholder="Name"/>
    </div>
  </div>
  
  <div class="form-group row">
    <label for="email" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
      <input type="email" runat="server" class="form-control" disabled="disabled" id="email" placeholder="Email"/>
    </div>
  </div>
  <div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="password" placeholder="Password"/>
    </div>
  </div>
      <div class="form-group row confirm" style="display:none">
    <label for="password" class="col-sm-2 col-form-label">Confirm Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="confirmpassword" placeholder="Password"/>
    </div>
  </div>


  <div class="form-group row">
    <div class="col-sm-4 offset-sm-4">
      <input type="button" id="btnEditAccountMode" class="btn btn-primary btn-block" value="EDIT ACCOUNT"/>
    </div>
  </div>
</div>
                  </div>

      </div>

  </div>
</div>
         </div>
                          </div>          



                          </div>
         
                    <!-- dito -->

                    </div>
                </div>
            </div>
                 </div>
          </div>
                 <input id="hidden_company" type="hidden" runat="server" />
          <input id="hidden_user" type="hidden" runat="server" />
          <input id="hiddent_agent" type="hidden" runat="server" />
    </form>
</body>
<script src="Scripts/bootstrap.min.js"></script>
</html>
