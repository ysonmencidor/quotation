﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SALESAGENT_WLO.ADMIN.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> - ADMIN - </title>
    <link rel="icon" type="image/x-icon" href="../img/ntbi-icon.png" />
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-grid.min.css" rel="stylesheet" />
    <script src="../Scripts/jquery-3.4.1.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
  <div class="container" style="margin-top:8%">
                 <div class="row">
                      <div class="col-md-4 offset-md-4  col-8 offset-2">
                     <img src="../img/nutra_logo_v1.png" class="img-responsive" style="margin-left:15%"/>
                         </div>
                 </div>
        
                 <br />
    <div class="row">
                      <div class="col-md-4 offset-md-4 col-8 offset-2">
     <asp:Panel DefaultButton="btnlogin" runat="server">
        <div class="form-group">
        <div class="row">
       <asp:TextBox runat="server" CssClass="form-control text-center" autocomplete="off" autofocus="autofocus" placeholder="Username" ID="txtusername" />
    </div>
        <div class="row" style="margin-top:5px">
    <asp:TextBox runat="server" TextMode="Password" autocomplete="off" CssClass="form-control text-center" Placeholder="Password" ID="txtpassword" /> 
     </div>
        <div class="row" style="margin-top:5px">
            <asp:LinkButton runat="server" ID="btnlogin" OnClick="btnlogin_Click" CssClass="btn btn-success btn-block">Login</asp:LinkButton>
       </div> 
            </div>
              </asp:Panel>
            </div>
        
             </div>
                     <div class="row">
                      <div class="col-md-4 offset-md-4 col-8 offset-2">
                <div id="divalert" visible="false" runat="server" class="alert alert-warning">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   Invalid Username or Password!
</div>
                 </div>
             </div>
            </div>
    </form>
</body>
</html>
