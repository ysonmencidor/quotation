﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SALESAGENT_WLO.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sign in</title>
     <link href="img/ntbi-icon.png" rel="shortcut icon" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/SigninCSS.css" rel="stylesheet" />
    <script src="Scripts/jquery-3.4.1.min.js"></script>
</head>
<body>
             <div class="wrapper fadeInDown">
              <div id="formContent">
                <!-- Tabs Titles -->
                <!-- Icon -->
                <div class="fadeIn first d-flex justify-content-center">
                  <img src="img/nutra_logo_v1.png" style="padding:15px;padding-right:0px" id="icon" alt="User Icon" />
                </div>

                <!-- Login Form -->
                <form id="form1" runat="server">
                  <asp:TextBox runat="server" ID="txtUsername" autofocus="autofocus" class="fadeIn second" name="login" placeholder="USERNAME"/>
                  <asp:TextBox runat="server" TextMode="Password" ID="txtPassword" class="fadeIn third" name="login" placeholder="PASSWORD"/>

                  <asp:Button runat="server" ID="btnLogin" OnClick="btnLogin_Click" CssClass="fadeIn fourth" Text="Log In"/>
                </form>

                <!-- Remind Passowrd -->
                <div id="formFooter">
                  <a class="underlineHover" href="#">Forgot Password?</a>
                </div>

              </div>
            </div>
</body>
    <script src="Scripts/bootstrap.min.js"></script>
</html>
