﻿$(document).ready(function () {
    var hidden_company = $('#hidden_company').val();
    var hidden_user = $('#hidden_user').val();
    var agent_id = $('#hiddent_agent').val();
    var selectedCompany;
    if (hidden_company == "1") {
        $("#IMG_LOGO").attr("src", "img/AVLILOGO.jpg");
        selectedCompany = 1;
    }
    else {
        $("#IMG_LOGO").attr("src", "img/APHILOGO.png");
            
        selectedCompany = 2;
    }

    var counter = 1;
    $(document).on("click", ".classAdd", function () { //
        var rowCount = $('.data-contact-person').length;

        rowCount + 1;
        counter += 1;
        var contactdiv = '<tr class="data-contact-person">' +
            '<td class="text-center"></td>' +
             '<td><input type="text"  name="s-name' + rowCount + '" class="form-control form-control-sm s-code01" /></td>' +
           '<td><select class="form-control form-control-sm s-name01" name="s-name01' + rowCount + '" id="ddlStock' + rowCount + '"></select></td>' +
            '<td><input type="number" min="1" name="s-qty' + rowCount + '" class="form-control form-control-sm s-qty01" /></td>' +
            '<td class="text-center"><input type="number"  name="s-price' + rowCount + '" class="form-control form-control-sm s-price01" /></td>' +
            '<td><input type="text"  name="s-uom' + rowCount + '" class="form-control form-control-sm s-uom01" /></td>' +
            '<td class="text-center"><button type="button" id="btnDelete"  data-toggle="tooltip" title="Hooray!" class="deleteRow btn btn-danger btn-sm"><span class="fa fa-remove"></span> </button></td>' +
            '</tr>';
        $('#wew').append(contactdiv); // Adding these controls to Main table class  

        $.ajax({
            type: "POST",
            url: 'WebService.asmx/GetStockCode',
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                var ddlStock = $('#ddlStock' + rowCount + '');
                ddlStock.empty().append('<option selected="selected" value="0">Please select</option>');
                $.each(r.d, function () {
                    ddlStock.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
            }
        });
    });

    $(document).on("click", ".deleteRow", function () {
        $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls  
    });

    $(document).on("change", ".s-name01", function () {
        //$(this).closest("tr").siblings().find('.s-name01').remove(); // closest used to remove the respective 'tr' in which I have my controls

        var STOCKCODE = $(this, ":selected");


        var a = $(this).closest("tr").find('td .s-code01');
        var b = $(this).closest("tr").find('td .s-uom01');
        var c = $(this).closest("tr").find('td .s-qty01');



        $.ajax({
            url: 'WebService.asmx/getUomByStockCode',
            data: { stockcode: STOCKCODE.val() },
            method: 'POST',
            dataType: 'xml',
            success: function (data) {
                var jqueryXml = $(data);
                $(a).val(jqueryXml.find('STOCKCODE').text());
                $(b).val(jqueryXml.find('UOM_').text());
                // $(c).val(1);
            },
            error: function (err) {
                alert(err);
            }
        });

        if (STOCKCODE.val() == '0') {
            $(c).val('');
        }
        else {
            $(c).val(1);
        }

        //alert(wew.val());
    });

    function getAllEmpData() {
        var data = [];
        $('tr.data-contact-person').each(function () {

            var name = $(this).find('.s-name01 option:selected').text();
            //console.log(Select_Stockcode);//  
            var code = $(this).find('.s-code01').val();
            var qty = $(this).find('.s-qty01').val();
            var price = $(this).find('.s-price01').val();
            var uom = $(this).find('.s-uom01').val();

            if (price == '') {
                price = 0;
            }

            var alldata = {
                'STOCKCODE': code, //FName as per Employee class name in .cs  
                'STOCKNAME': name, //LName as per Employee class name in .cs  
                'QTY': qty, //EmailId as per Employee class name in .cs   
                'PRICE': price, //EmailId as per Employee class name in .cs   
                'UOM': uom //EmailId as per Employee class name in .cs   
            }
            data.push(alldata);
        });
        // console.log(data);//  
        return data;
    };

    $(document).on("click", ".SAVEQUOTATION", function () { //

        var QT_details = JSON.stringify(getAllEmpData());
        var ddCustomer = $('#ddlCustomer').val();
        var ddCustomerName = $("#ddlCustomer option:selected").text();
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();


        var data = [];
        var qt_detai = {
            'CUSTOMER': ddCustomer,
            'AGENT': hidden_user,
            'DATE_SAVE': date,
            'CUSTOMERNAME': ddCustomerName,
            'AGENT_ID': agent_id,
            'COMPANY': selectedCompany
        }
        data.push(qt_detai);

        var QT_MAIN = JSON.stringify(data);

        //console.log(QT_details);
        //console.log(QT_MAIN);
        //console.log(ddCustomer);
        if (ddCustomer == "0") {
            $.alert({
                autoClose: 'ok|3000',
                title: false,
                content: '</br><div class="alert alert-warning text-center"><span class="fa fa-warning"></span> PLEASE SELECT CUSTOMER FIRST </div>'
            });
            //alert("");
        }
        else if(ddCustomer == "Loading...")
        {
            $.alert({
                autoClose: 'ok|3000',
                title: false,
                content: '</br><div class="alert alert-warning text-center"><span class="fa fa-warning"></span> PLEASE SELECT CUSTOMER FIRST </div>'
            });
        }
        else {

            $.ajax({
                url: 'WebService.asmx/SaveData',//Home.aspx is the page   
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ 'QT_DETAILS': QT_details, 'QT_MAIN': QT_MAIN }),
                success: function () {
                    $("#wew").find("tr:gt(1)").remove();
                    $('.s-name01').prop('selectedIndex', 0);
                    $('.s-code01').val('');
                    $('.s-qty01').val('');
                    $('.s-price01').val('');
                    $('.s-uom01').val('');

                   $.alert({
                        autoClose: 'ok|3000',
                        title: false,
                        content: '</br><div class="alert alert-success text-center"><i class="fa fa-check fa-4x"></i></br> <em>SAVE QUOTATION <strong>SUCCESSFULLY!</strong></em> </div>'
                    });
                },
                error: function () {
                    alert("Error while inserting data");
                }
            });
        }


    });

    $.ajax({
        type: "POST",
        url: 'WebService.asmx/GetStockCode',
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlstock = $('#ddlStock');
            ddlstock.empty().append('<option selected="selected" value="0">Please select</option>');
            $.each(r.d, function () {
                ddlstock.append($("<option></option>").val(this['Value']).html(this['Text']));


            });
        }
    });


    $('#ddlCustomer').prepend($('<option> Loading... </option>'));

    $.ajax({
        type: "POST",
        url: 'WebService.asmx/GetCustomerList',
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlCustomer = $('#ddlCustomer');


            ddlCustomer.empty().append('<option selected="selected" value="0">Please select</option>');
            $.each(r.d, function () {
                ddlCustomer.append($("<option></option>").val(this['Value']).html(this['Text']));


            });
        }
    });



});