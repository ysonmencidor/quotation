﻿var tableUser;
$(document).ready(function () {

    loadUsers();

    function loadUsers() {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../WebService.asmx/GetUsers",
            data: '{}',
            success: function (data) {
                tableUser = $('#tblUsers').DataTable({
                    //  "lengthChange": false,
                    //  "processing": true,         
                    // "serverSide": false,  
                    destroy: true,
                    stateSave: true,
                    data: data,
                    columns: [
                        { 'data': 'USERNAME' },
                        { 'data': 'EMAIL' },
                        { 'data': 'NAME' },
                        {
                            'data': 'COMPANY', 'render': function (company) {
                                if (company == 01) {
                                    return 'AVLI';
                                }
                                else {
                                    return 'APHI';
                                }

                            }
                        }
                    ],
                    order: [[1, 'desc']]
                });
            }
        });
    }



    $("#btnSave").on("click", function (e) {

        e.preventDefault();

        var company = $("input[name='gridRadios']:checked").val();
        var username = $("#username").val();
        var password = $("#password").val();
        var fullname = $("#name").val();
        var email = $("#email").val();

        var data = [];
        var U_detail = {
            'USERNAME': username,
            'PASSWORD': password,
            'COMPANY': company,
            'EMAIL': email,
            'NAME': fullname
        }
        data.push(U_detail);

        var USER_DATA = JSON.stringify(data);

        $.ajax({
            url: '../WebService.asmx/SaveUserData',//Home.aspx is the page   
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ 'USER_DATA': USER_DATA }),
            success: function () {
                alert("Create Account Successfully");
                console.log("SUCCESS");
                $("#username").val('');
                $("#password").val('');
                $("#name").val('');
                $("#email").val('');
                $('#collapseOne').collapse('hide');
                loadUsers();
            },
            error: function () {
                alert("Error while inserting data");
            }
        });

    });

    $(".collapse.show").each(function () {
        $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });

    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });
});
