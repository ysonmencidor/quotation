﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ADMIN/AdminMaster.Master" AutoEventWireup="true" CodeBehind="primary.aspx.cs" Inherits="SALESAGENT_WLO.ADMIN.primary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/ADMINAGENT.js"></script>
    <script src="../js/ADMINWLO.js"></script>
   
     <script src="../js/ADMINASSIGNMENT.js"></script>

    <script>
        $(document).ready(function () {

            $("#btnAssign").on("click", function (e) {

                e.preventDefault();
                var agent = $("#assignAGENT option:selected");
                var wlo = $("#assignWLO option:selected");

                //console.log(agent,department,wlo)

                var data = [];
                var U_detail = {
                    'AGENT_ID_FK': agent.val(),
                    'WLO_ID_FK': wlo.val()
                }
                data.push(U_detail);

                var FINAL_ASSIGNMENT = JSON.stringify(data);

                $.ajax({
                    url: '../WebService.asmx/SaveAssignment',//Home.aspx is the page   
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({ 'final': FINAL_ASSIGNMENT }),
                    success: function () {
                        alert("Assignment Successfully");
                        //console.log("SUCCESS");
                        LoadAssignment();
                        $('#assignAGENT').prop('selectedIndex', 0);
                        $('#assignWLO').prop('selectedIndex', 0);
                      //  $("#deptCode").val('');
                       // $("#deptDesc").val('');
                       // $('#collapseOne2').collapse('hide');

                    },
                    error: function () {
                        alert("Error while inserting data");
                    }
                });

            });

            function LoadAssignment() {

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "../WebService.asmx/GetAssigned",
                    data: '{}',
                    success: function (data) {
                        datatableVariable = $('#tblAssigned').DataTable({
                            //  "lengthChange": false,
                            //  "processing": true,         
                            // "serverSide": false,  
                            destroy: true,
                            stateSave: true,
                            data: data,
                            columns: [
                                { 'data': 'COMPANY' },
                                { 'data': 'AGENT_NAME' },
                                { 'data': 'AGENT_EMAIL' },
                                { 'data': 'WLO_NAME' },
                                { 'data': 'WLO_EMAIL' }
                            ],
                        });

                    }
                });
            }
            LoadAssignment();
        });

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     
    <div class="row">
        <div class="col-12 col-md-3 col-lg-3">
<div class="nav flex-column nav-pills shadow-lg border-danger" id="v-pills-tab" role="tablist" aria-orientation="vertical">
  <a class="nav-link active" id="v-pills-agent-tab" data-toggle="pill" href="#v-pills-agent" role="tab" aria-controls="v-pills-home" aria-selected="true"><span class="fa fa-user-secret"></span> Agent Account</a>
  <a class="nav-link" id="v-pills-wlo-tab" data-toggle="pill" href="#v-pills-wlo" role="tab" aria-controls="v-pills-home" aria-selected="false"><span class="fa fa-user-circle"></span> WLO Account</a>
  <a class="nav-link" id="v-pills-assign-tab" data-toggle="pill" href="#v-pills-assign" role="tab" aria-controls="v-pills-assign" aria-selected="false"><span class="fa fa-warning"></span> Notification</a>
  <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><span class="fa fa-gear"></span> Settings</a>
  <asp:LinkButton runat="server" ID="btnOut" OnClick="btnOut_Click" class="nav-link"><span class="fa fa-sign-out"></span> Sign out</asp:LinkButton>
</div>
            </div>
        <div class="col-12 col-md-9 col-lg-9">
<div class="tab-content">

  <div class="tab-pane fade show active pillsPublicClass" id="v-pills-agent" role="tabpanel" aria-labelledby="v-pills-agent-tab">
   
    <div id="accordionExample">
        <div class="card">
            <div class="card-header"">
                <a data-toggle="collapse" href="#collapseOne"> <span class="fa fa-users"></span> Create User <i class="pull-right fa fa-plus"></i></a>									
            </div>
            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
             
<div role="form" id="CreateForm">
  <div class="form-group row">
    <label for="email" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="email" placeholder="Email"/>
    </div>
  </div>
      <div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="name" placeholder="Name"/>
    </div>
  </div>
  <div class="form-group row">
    <label for="username" class="col-sm-2 col-form-label">Username</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="username" placeholder="Username"/>
    </div>
  </div>  <div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">Password</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="password" placeholder="Password"/>
    </div>
  </div>
  <fieldset class="form-group">
    <div class="row">
      <label class="col-form-label col-sm-2 pt-0">Company</label>
      <div class="col-sm-10">
        <div class="form-check">
          <input class="form-check-input" type="radio" name="gridRadios" id="company01" value="1" checked/>
          <label class="form-check-label" for="company01">
            AVLI
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="gridRadios" id="company02" value="2"/>
          <label class="form-check-label" for="company02">
            APTHEALTH
          </label>
        </div>
      </div>
    </div>
  </fieldset>

  <div class="form-group row">
    <div class="col-sm-4 offset-sm-4">
      <button type="submit" id="btnSave" class="btn btn-primary btn-block">Create Account</button>
    </div>
  </div>
</div>  
                </div>
            </div>
        

    </div>
        <br />
        <div class="card">
  <div class="card-header">
   <i class="fa fa-table"> ACCOUNTS</i>
  </div>
  <div class="card-body">
 
     <table class="table table-striped table-bordered table-responsive-sm nowrap" id="tblUsers">
         <thead>
             <tr>
                 <th>USERNAME</th>
                 <th>EMAIL</th>
                 <th>NAME</th>
                 <th>COMPANY</th>
             </tr>
         </thead>
         <tbody>

         </tbody>
     </table>

  </div>
</div>
</div>
   </div>
     <div class="tab-pane fade pillsPublicClass" id="v-pills-wlo" role="tabpanel" aria-labelledby="v-pills-wlo-tab">
        <div id="accordionExample2">
        <div class="card">
            <div class="card-header"">
                <a data-toggle="collapse" href="#collapseOne1"> <span class="fa fa-users"></span> Create User <i class="pull-right fa fa-plus"></i></a>									
            </div>
            <div id="collapseOne1" class="collapse" data-parent="#accordionExample2">
                <div class="card-body">
             
<div role="form" id="CreateFormWLO">
  <div class="form-group row">
    <label for="emailwlo" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="emailwlo" placeholder="Email"/>
    </div>
  </div>
      <div class="form-group row">
    <label for="namewlo" class="col-sm-2 col-form-label">Department</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="namewlo" placeholder="Department Name"/>
    </div>
  </div>
  <div class="form-group row">
    <label for="usernamewlo" class="col-sm-2 col-form-label">Username</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="usernamewlo" placeholder="Username"/>
    </div>
  </div> 
     <div class="form-group row">
    <label for="passwordwlo" class="col-sm-2 col-form-label">Password</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="passwordwlo" placeholder="Password"/>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-4 offset-sm-4">
      <button type="submit" id="btnSaveWLO" class="btn btn-primary btn-block">Create Account</button>
    </div>
  </div>
</div>  
                </div>
            </div>
        

    </div>
        <br />
        <div class="card">
  <div class="card-header">
   <i class="fa fa-table"> ACCOUNTS</i>
  </div>
  <div class="card-body">
 
     <table class="table table-striped table-bordered table-responsive-sm nowrap" style="width:100%" id="tblwloUsers">
         <thead>
             <tr>
                 <th>USERNAME</th>
                 <th>EMAIL</th>
                 <th>Department</th>
             </tr>
         </thead>
         <tbody>

         </tbody>
     </table>

  </div>
</div>
</div>
  
  </div>
  <div class="tab-pane pillsPrivateClass" id="v-pills-assign" role="tabpanel" aria-labelledby="v-pills-assign-tab">
<div class="card">
  <div class="card-header">
    <i class="fa fa-address-book"> ASSIGNMENT</i> 
  </div>
  <div class="card-body">

<div role="form" id="assignForm">
      <div class="form-group row">
    <label for="assignAGENT" class="col-sm-2 col-form-label">AGENT</label>
    <div class="col-sm-10">
           <select class="form-control" id="assignAGENT"></select>
    </div>
  </div>
  <div class="form-group row">
    <label for="assignWLO" class="col-sm-2 col-form-label">REPORTING TO</label>
    <div class="col-sm-10">
           <select class="form-control" id="assignWLO"></select>
    </div>
  </div> 


  <div class="form-group row">
    <div class="col-sm-4 offset-sm-4">
      <button type="submit" id="btnAssign" class="btn btn-primary btn-block">ASSIGNED</button>
    </div>
  </div>
</div>  
  </div>
</div>

  </div>
  <div class="tab-pane fade pillsPublicClass" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
</div>



      </div>  

</div>
    <br />
    <div class="row">
        <div class="col-md-12 col-lg-12 col-12">
    
            <div class="tab-content">           
  <div class="tab-pane fade pillsPrivateClass" id="v-pills-assign" role="tabpanel" aria-labelledby="v-pills-assign-tab">

      <div class="card">
  <div class="card-header">
  <i class="fa fa-check-square-o"> ASSIGNED TABLE</i>
  </div>
  <div class="card-body">
      <table id="tblAssigned" class="table table-striped table-bordered table-responsive-sm nowrap " style="width:100%">
          <thead>
              <tr>
                  <th>COMPANY</th>
                  <th>ASSIGEND AGENT</th>
                  <th>AGENT EMAIL</th>
                  <th>REPORTING TO</th>
                  <th>WLO EMAIL</th>
              </tr>
          </thead>
      </table>
  </div>
</div>

      



      </div>
            </div>



        </div>
        </div>
</asp:Content>
