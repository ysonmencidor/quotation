﻿<%@ Page Title="" Language="C#" MasterPageFile="~/A_WLO/WLO.Master" AutoEventWireup="true" CodeBehind="WLOMAINPAGE.aspx.cs" Inherits="SALESAGENT_WLO.A_WLO.WLOMAINPAGE" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>
        $(document).ready(function () {

            var table;

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "../WebService.asmx/GetQuotationsWLO",
                data: '{}',
                success: function (data) {
                    table = $('#QuotTable').DataTable({
                        //  "lengthChange": false,
                        //  "processing": true,         
                        // "serverSide": false,  
                        destroy: true,
                        stateSave: true,
                        data: data,
                        columns: [
                            {
                                "className": 'details-control',
                                "orderable": false,
                                "data": null,
                                "defaultContent": ''
                            }, {
                                "data": 'QT_ID',
                                "visible": false,
                                "searchable": false
                            },
                            { 'data': 'QTCODE' },
                            {
                                'data': 'DATE_SAVE', 'render': function (date) {
                                    var date = new Date(parseInt(date.substr(6)));
                                    var month = date.getMonth() + 1;
                                    return month + "-" + date.getDate() + "-" + date.getFullYear();
                                }
                            },
                            { 'data': 'CUSTOMERNAME' },
                            { 'data': 'AGENT' }
                        ],
                        "order": [[1, 'desc']]
                    });

                    $('#QuotTable tbody').on('click', 'td.details-control', function () {
                        var tr = $(this).closest('tr');
                        var row = table.row(tr);

                        console.log("CLICKED!");

                        if (row.child.isShown()) {
                            // This row is already open - close it
                            $('div.slider', row.child()).slideUp(function () {
                                row.child.hide();
                                tr.removeClass('shown bg-success text-white');
                            });
                        }
                        else {

                            // Open this row
                            row.child(format(row.data()), 'no-padding').show();
                            tr.addClass('shown bg-success text-white');

                            $('div.slider', row.child()).slideDown();
                        }

                    });
                }


            });

     

            function format(d) {

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "../WebService.asmx/GetQuotationDetails",
                    data: { 'Quot_ID_MAIN': d.QT_ID },
                    success: function (data) {
                        var datatableVariable = $('#qtDetails' + d.QT_ID + '').DataTable({
                            "lengthChange": false,
                            "searching": false,
                            "paging": false,
                            "ordering": false,
                            "bInfo": false,
                            "responsive": true,
                            data: data,
                            columns: [
                                {
                                    "ordering": false,
                                    "data": null,
                                    "defaultContent": ''
                                },
                                { 'data': 'STOCKCODE' },
                                { 'data': 'STOCKNAME' },
                                { 'data': 'QTY' },
                                { 'data': 'UOM' },
                                { 'data': 'PRICE' },
                            ],
                            "order": [[1, 'asc']],
                            "footerCallback": function (row, data, start, end, display) {
                                var api = this.api(), data;

                                // Remove the formatting to get integer data for summation
                                var intVal = function (i) {
                                    return typeof i === 'string' ?
                                        i.replace(/[\$,]/g, '') * 1 :
                                        typeof i === 'number' ?
                                        i : 0;
                                };

                                
                                // Total over this page
                                pageTotal = api
                                    .column(5, { page: 'current' })
                                    .data()
                                    .reduce(function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0);

                                // Update footer
                                $(api.column(5).footer()).html(pageTotal);
                            }
                        });
                    }

                });

                // `d` is the original data object for the row
                return '<div class="slider"><table class="table table-borderless table-hover css-serial" id="qtDetails' + d.QT_ID + '"  style="width:100%;">' +
                    '<thead class="bg-warning"><tr>' +
                        '<th>#</th>' +
                        '<th>STOCKCODE</th>' +
                        '<th>STOCKNAME</th>' +
                        '<th>QTY</th>' +
                        '<th>UOM</th>' +
                        '<th>PRICE</th>' +
                    '</tr></thead>' +
            '<tfoot><tr class="table-primary"><th colspan="5" style="text-align:right">Total Amount :</th><th></th></tr></tfoot>'
                '</table></div>';


            }
        });

       
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <br />

                                         <div class="row">
                              <div class="col-12 col-lg-12 col-md-12">
 <div class="card shadow">
  <div class="card-header">
    <ul class="nav nav-tabs card-header-tabs">
      <li class="nav-item">
        <a class="nav-link active" id="v-pills-1st-tab" data-toggle="pill" href="#pills1" role="tab" aria-selected="true" >Quotations</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="v-pills-2nd-tab" data-toggle="pill" href="#pills2" role="tab" aria-selected="false">Settings</a>
      </li>
      <li class="nav-item">
        <asp:LinkButton runat="server" class="nav-link" ID="btnLgOut" OnClick="btnLgOut_Click"><span class="fa fa-sign-out"></span> Log Out</asp:LinkButton>
      </li>
    </ul>
  </div>
  <div class="card-body">

      <div class="tab-content">
        <div class="tab-pane fade show active" id="pills1" role="tabpanel" aria-labelledby="v-pills-1st-tab">
          
              <table id="QuotTable" class="table table-striped table-bordered table-responsive-sm" style="width:100%">

                                     <thead>
                                         <tr>
                                             <th style="width:5%"></th>
                                             <th style="display:none"></th>
                                             <th style="width:15%">QTCODE</th>
                                             <th style="width:15%">DATE</th>
                                             <th style="width:55%">CUSTOMER</th>
                                             <th style="width:10%">AGENT</th>
                                         </tr>
                                     </thead>
                      
                                         </table>
            </div>

           <div class="tab-pane fade" id="pills2" role="tabpanel" aria-labelledby="v-pills-2nd-tab">
               2nd
            </div>
          </div>
  </div>
</div>
 
                                  </div> <!-- end of 12 -->
                    </div> <!-- end of row -->
   
     
    
            
</asp:Content>
