﻿$(document).ready(function () {

    $(".nav-link").click(function () {
        if ($(this)[0].id == 'v-pills-assign-tab') {
            $(".pillsPrivateClass").each(function () {
                if ($(this).hasClass('active')) { /* */ } else {
                    $(this).addClass('active');
                    $(this).addClass('show');
                }
            });
            $(".pillsPublicClass").each(function () {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $(this).removeClass('show');
                }
            });
        } else {
            $(".pillsPrivateClass").each(function () {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $(this).removeClass('show');
                }
            });
        }
    });

    function loadAgentList() {
        $.ajax({
            type: "POST",
            url: '../WebService.asmx/GetAgentListAdmin',
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                var ddlAgent = $('#assignAGENT');
                ddlAgent.empty().append('<option selected="selected" value="0">SELECT AGENT</option>');
                $.each(r.d, function () {
                    ddlAgent.append($("<option></option>").val(this['Value']).html(this['Text']));

                });
            }
        });
    }


    function loadWLOList() {
        $.ajax({
            type: "POST",
            url: '../WebService.asmx/GetWloListAdmin',
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                var ddlassignWLO = $('#assignWLO');
                ddlassignWLO.empty().append('<option selected="selected" value="0">SELECT WLO</option>');
                $.each(r.d, function () {
                    ddlassignWLO.append($("<option></option>").val(this['Value']).html(this['Text']));

                });
            }
        });
    }

    loadAgentList();
    loadWLOList();
});