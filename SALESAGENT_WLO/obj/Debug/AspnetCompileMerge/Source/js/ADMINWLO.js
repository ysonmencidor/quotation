﻿var tableUser2;
$(document).ready(function () {

    loadUsers();

    function loadUsers() {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../WebService.asmx/GetUsersWLO",
            data: '{}',
            success: function (data) {
                tableUser2 = $('#tblwloUsers').DataTable({
                    //  "lengthChange": false,
                    //  "processing": true,         
                    // "serverSide": false,  
                    destroy: true,
                    stateSave: true,
                    data: data,
                    columns: [
                        { 'data': 'USERNAME' },
                        { 'data': 'EMAIL' },
                        { 'data': 'NAME' }
                    ],
                    order: [[1, 'desc']]
                });
            }
        });
    }


    $("#btnSaveWLO").on("click", function (e) {

        e.preventDefault();
        var username = $("#usernamewlo").val();
        var password = $("#passwordwlo").val();
        var fullname = $("#namewlo").val();
        var email = $("#emailwlo").val();

        var data = [];
        var U_detail = {
            'USERNAME': username,
            'PASSWORD': password,
            'EMAIL': email,
            'NAME': fullname
        }
        data.push(U_detail);

        var USER_DATA = JSON.stringify(data);

        $.ajax({
            url: '../WebService.asmx/SaveUserDataWLO',//Home.aspx is the page   
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ 'USER_DATA': USER_DATA }),
            success: function () {
                alert("Create Account Successfully");
                console.log("SUCCESS");
                $("#usernamewlo").val('');
                $("#passwordwlo").val('');
                $("#namewlo").val('');
                $("#emailwlo").val('');
                $('#collapseOne1').collapse('hide');
                loadUsers();
            },
            error: function () {
                alert("Error while inserting data");
            }
        });

    });

    $(".collapse.show").each(function () {
        $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });

    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });
});