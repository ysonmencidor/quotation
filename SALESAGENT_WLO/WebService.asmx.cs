﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;
using FirebirdSql.Data.FirebirdClient;
using FirebirdSql.Data;
using System.Configuration;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Converters;
using System.Web.Script.Services;

namespace SALESAGENT_WLO
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        public static string Constr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;


        //WLO PAGE REGION

        [WebMethod(EnableSession = true)]
        public void UpdateProfileWlo(string Email, string Password)
        {

            string UwerWLO = HttpContext.Current.Session["ID_WLO"].ToString();
            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE WLO_ACCOUNT set EMAIL = '" + Email + "',PASSWORD = '" + Password + "' WHERE ID = '" + UwerWLO + "'", con))
                {

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();
                    //string name = System.Environment.MachineName;

                    InsertTrail(UwerWLO, " Edit User Profile", "Update");
                }
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public USERDETAIL getWLOUserDetails()//WebMethod to get the uom USING STOCKCODE  
        {
            //string agent_ID = HttpContext.Current.Session["agent_id"].ToString();
            string UwerWLO = HttpContext.Current.Session["ID_WLO"].ToString();
            USERDETAIL user = new USERDETAIL();

            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM WLO_ACCOUNT WHERE ID = @ID", con))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@ID", UwerWLO);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            user.NAME = sdr["NAME"].ToString();
                            user.EMAIL = sdr["EMAIL"].ToString();
                            user.PASSWORD = sdr["PASSWORD"].ToString();

                        }
                    }
                    con.Close();
                }

            }
            return user;
        }

        #region
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public String GetQuotationsWLO()
        {
            string UwerWLO = HttpContext.Current.Session["ID_WLO"].ToString();
            try
            {
                //string qneConnection = QNEConnectionString.ChooseConnection(company_code);
                string queryString = @"SELECT A.* FROM QUOTATION AS A 
                            JOIN ASSIGNMENT AS B
                            ON A.AGENT_ID = B.AGENT_ID_FK
                            JOIN WLO_ACCOUNT AS C
                            ON B.WLO_ID_FK = C.ID
                            WHERE C.ID = '" + UwerWLO + "'";
                DataTable dt = new DataTable();

                using (SqlConnection fbConnection = new SqlConnection(Constr))
                {
                    fbConnection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(queryString, fbConnection);
                    da.Fill(dt);
                    fbConnection.Close();
                }
                return "{\"aaData\":" + DataTableToJSONWithJSONNet(dt) + "}";
            }

            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("error: " + ex.Message);
                Context.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                Context.Response.StatusDescription = ex.Message;
                return null;
            }
        }

        #endregion

        //ADMIN PAGE REGION
        #region  

        [WebMethod]
        public void DeactivateSRUser(int id)
        {

            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE account set DEACTIVATED = '1' WHERE ACCOUNT_ID = '" + id + "'", con))
                {

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();
                    //string name = System.Environment.MachineName;

                    InsertTrail("Admin", " Deactivate SR User id " + id, "Update");
                }
            }
        }

        [WebMethod]
        public void ReactivateSRUser(int id)
        {

            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE account set DEACTIVATED = '0' WHERE ACCOUNT_ID = '" + id + "'", con))
                {

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();
                    //string name = System.Environment.MachineName;

                    InsertTrail("Admin", " Reactivate SR User id " + id, "Update");
                }
            }
        }


        [WebMethod]
        public void UpdateWloUser(string username, string name, string email,int id)
        {

            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE WLO_ACCOUNT set USERNAME = '" + username + "',NAME = '" + name + "',EMAIL = '" + email + "' WHERE ID = '" + id + "'", con))
                {

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();
                    //string name = System.Environment.MachineName;

                    InsertTrail("Admin", " Edit User Profile Id +" + id.ToString(), "Update");
                }
            }
        }

        [WebMethod]
        public void DeactivateWloUser(int id)
        {

            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE WLO_ACCOUNT set DEACTIVATED = '1' WHERE ID = '" + id + "'", con))
                {

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();
                    //string name = System.Environment.MachineName;

                    InsertTrail("Admin", " Deactivate User id "+id, "Update");
                }
            }
        }

        [WebMethod]
        public void ReactivateWloUser(int id)
        {

            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE WLO_ACCOUNT set DEACTIVATED = '0' WHERE ID = '" + id + "'", con))
                {

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();
                    //string name = System.Environment.MachineName;

                    InsertTrail("Admin", " Activate User Id " + id, "Update");
                }
            }
        }

        [WebMethod]
        public void UpdateAssignment(string ass_ID, string agent_FK, string wlo_FK)
        {

            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE ASSIGNMENT set AGENT_ID_FK = '" + agent_FK + "',WLO_ID_FK = '" + wlo_FK + "' WHERE ASISGNED_ID = '" + ass_ID + "'", con))
                {

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();
                    //string name = System.Environment.MachineName;

                    //InsertTrail(Agent_ID, " Edit User Profile", "Update");
                }
            }
        }

        [WebMethod]
        public void DeleteAssignmentAdmin(string ass_ID)
        {

            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("DELETE FROM ASSIGNMENT WHERE ASISGNED_ID = '" + ass_ID + "'", con))
                {

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();
                    //string name = System.Environment.MachineName;

                    //InsertTrail(Agent_ID, " Edit User Profile", "Update");
                }
            }
        }


        [WebMethod(enableSession: true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public ASSIGNMENT getAssignmentByassID(int assID)  
        {

            ASSIGNMENT ass = new ASSIGNMENT();

            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM ASSIGNMENT WHERE ASISGNED_ID = @ASS", con))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@ASS", assID);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            ass.AGENT_ID_FK = (Int32)sdr["AGENT_ID_FK"];
                            ass.WLO_ID_FK = (Int32)sdr["WLO_ID_FK"];

                        }
                    }
                    con.Close();
                }

            }
            return ass;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public void GetAssigned()
        {
                    //string UserAgent = HttpContext.Current.Session["userName"].ToString();
            List<ASSIGN> ListAss = new List<ASSIGN>();
            string Query = "";
            Query = @"SELECT D.COMPANY,D.NAME AS AGENT_NAME,D.EMAIL AS AGENT_EMAIL,C.NAME AS WLO_NAME,C.EMAIL AS WLO_EMAIL,A.ASISGNED_ID,A.AGENT_ID_FK FROM ASSIGNMENT AS A
                            LEFT JOIN WLO_ACCOUNT AS C
                            ON A.WLO_ID_FK = C.ID
                            LEFT JOIN account AS D
                            on A.AGENT_ID_FK = D.ACCOUNT_ID
                            ORDER BY D.COMPANY ASC";
            
            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand(Query, con))
                {

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;
                  if (con.State == ConnectionState.Closed)

                    {
                        con.Open();
                    }
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        ASSIGN list = new ASSIGN();
                        list.ASISGNED_ID = (Int32)dr["ASISGNED_ID"];
                        list.COMPANY = dr["COMPANY"].ToString();
                        list.AGENT_NAME = dr["AGENT_NAME"].ToString();
                        list.AGENT_EMAIL = dr["AGENT_EMAIL"].ToString();
                        list.WLO_NAME = dr["WLO_NAME"].ToString();
                        list.WLO_EMAIL = dr["WLO_EMAIL"].ToString();
                        list.AGENT_ID_FK = (Int32)dr["AGENT_ID_FK"];
                        ListAss.Add(list);
                    }
                    con.Close();

                }
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            //return js.Serialize(listQuot);
            Context.Response.Write(js.Serialize(ListAss));
        }
        [WebMethod(EnableSession = true)]
        public string SaveAssignment(string final)//WebMethod to Save the data  
        {
            var serializeData_UserData = JsonConvert.DeserializeObject<List<ASSIGN>>(final);

              using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                foreach (var data in serializeData_UserData)
                {
                    using (SqlCommand cmd = new SqlCommand("INSERT INTO ASSIGNMENT VALUES (@AGENT_ID_FK,@WLO_ID_FK)", con))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@AGENT_ID_FK", data.AGENT_ID_FK);
                        cmd.Parameters.AddWithValue("@WLO_ID_FK", data.WLO_ID_FK);
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }

            }
            return null;
        }
        [WebMethod(EnableSession = true)]
        public List<ListItem> GetAgentListAdmin()
        {

            string query = @"SELECT * FROM account";
            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    List<ListItem> agents = new List<ListItem>();
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            agents.Add(new ListItem
                            {
                                Value = sdr["ACCOUNT_ID"].ToString(),
                                Text = sdr["USERNAME"].ToString() + " - " + sdr["NAME"].ToString()
                            });
                        }
                    }
                    con.Close();
                    return agents;
                }
            }
        }
        
        [WebMethod(EnableSession = true)]
        public List<ListItem> GetWloListAdmin()
        {

            string query = @"SELECT * FROM WLO_ACCOUNT";
            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    List<ListItem> depts = new List<ListItem>();
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            depts.Add(new ListItem
                            {
                                Value = sdr["ID"].ToString(),
                                Text = sdr["USERNAME"].ToString() + " - " + sdr["NAME"].ToString()
                            });
                        }
                    }
                    con.Close();
                    return depts;
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public void GetUsers()
        {
            //string UserAgent = HttpContext.Current.Session["userName"].ToString();
            List<USERDETAIL> listUser = new List<USERDETAIL>();

            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM account", con))
                {

                    cmd.CommandType = System.Data.CommandType.Text; cmd.Connection = con;

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        USERDETAIL list = new USERDETAIL();
                        list.USERNAME = dr["USERNAME"].ToString();
                        list.EMAIL = dr["EMAIL"].ToString();
                        list.COMPANY = dr["COMPANY"].ToString();
                        list.NAME = dr["NAME"].ToString();
                        list.DEACTIVATED = (bool)dr["DEACTIVATED"];
                        list.ID = (Int32)dr["ACCOUNT_ID"];
                        listUser.Add(list);
                    }
                    con.Close();

                }
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            //return js.Serialize(listQuot);
            Context.Response.Write(js.Serialize(listUser));
        }
            

        [WebMethod(EnableSession = true)]
        public void GetUsersWLO()
        {
            //string UserAgent = HttpContext.Current.Session["userName"].ToString();
            List<USERDETAIL> listUser = new List<USERDETAIL>();

            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM WLO_ACCOUNT", con))
                {

                    cmd.CommandType = System.Data.CommandType.Text; cmd.Connection = con;

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        USERDETAIL list = new USERDETAIL();
                        list.USERNAME = dr["USERNAME"].ToString();
                        list.EMAIL = dr["EMAIL"].ToString();
                        list.NAME = dr["NAME"].ToString();
                        list.ID = int.Parse(dr["ID"].ToString());
                        list.DEACTIVATED = (bool)dr["DEACTIVATED"];
                        listUser.Add(list);
                    }
                    con.Close();

                }
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            //return js.Serialize(listQuot);
            Context.Response.Write(js.Serialize(listUser));
        }


        [WebMethod(enableSession: true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public USERDETAIL GetWLObyId(int wloId)
        {

            USERDETAIL ass = new USERDETAIL();

            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM WLO_ACCOUNT WHERE ID = @id", con))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", wloId);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            ass.ID = (Int32)sdr["ID"];
                            ass.EMAIL = sdr["EMAIL"].ToString();
                            ass.NAME = sdr["NAME"].ToString();
                            ass.USERNAME = sdr["USERNAME"].ToString();

                        }
                    }
                    con.Close();
                }

            }
            return ass;
        }

        [WebMethod(enableSession: true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public USERDETAIL GetUserById(int userid)
        {

            USERDETAIL ass = new USERDETAIL();

            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM account WHERE ACCOUNT_ID = @id", con))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", userid);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            ass.ID = (Int32)sdr["ACCOUNT_ID"];
                            ass.EMAIL = sdr["EMAIL"].ToString();
                            ass.NAME = sdr["NAME"].ToString();
                            ass.USERNAME = sdr["USERNAME"].ToString();
                            ass.COMPANY = sdr["COMPANY"].ToString();
                            ass.NUMBER = sdr["CONTACTNUMBER"].ToString();
                            
                        }
                    }
                    con.Close();
                }

            }
            return ass;
        }

        [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public string CheckUserExist(string Username)//WebMethod to Save the data  
        {
            bool checkResult = false;

            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                 if (Username != "")
                  {
                   
                            using (SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM account WHERE USERNAME = '"+Username+"'", con))
                            {
                                    DataTable dt = new DataTable();
                                    da.Fill(dt);
                                    int no = dt.Rows.Count;
                                    if (no > 0)
                                    {
                                      checkResult = true;
                                    }
                                    else
                                    {
                                      checkResult = false;
                                    }
                            }           
                 }
            }

            return JsonConvert.SerializeObject(new { result = checkResult });

        }
        [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public string SaveUserData(string USER_DATA)//WebMethod to Save the data  
        {
            var data2 = JsonConvert.DeserializeObject<List<AddUser>>(USER_DATA);

            string action = "";
            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                foreach (var data in data2)
                {
                    if (data.ID > 0)
                    {
                        using (SqlCommand cmd = new SqlCommand("UPDATE account SET USERNAME = @USERNAME,CONTACTNUMBER = @NUMBER,COMPANY = @COMPANY,EMAIL = @EMAIL,NAME = @NAME WHERE ACCOUNT_ID = '"+data.ID+"'", con))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@USERNAME", data.USERNAME);
                            cmd.Parameters.AddWithValue("@NUMBER", data.CONTACTNUMBER);
                            cmd.Parameters.AddWithValue("@COMPANY", data.COMPANY);
                            cmd.Parameters.AddWithValue("@EMAIL", data.EMAIL);
                            cmd.Parameters.AddWithValue("@NAME", data.NAME);
                            if (con.State == ConnectionState.Closed)
                            {
                                con.Open();
                            }
                            cmd.ExecuteNonQuery();
                            con.Close();
                            action = "UPDATE";
                        }
                    }
                    else
                    {

                        using (SqlCommand cmd = new SqlCommand("INSERT INTO account VALUES (@USERNAME,'abc123',@COMPANY,@EMAIL,@NAME,@NUMBER,'false')", con))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@USERNAME", data.USERNAME);
                            cmd.Parameters.AddWithValue("@NUMBER", data.CONTACTNUMBER);
                            cmd.Parameters.AddWithValue("@COMPANY", data.COMPANY);
                            cmd.Parameters.AddWithValue("@EMAIL", data.EMAIL);
                            cmd.Parameters.AddWithValue("@NAME", data.NAME);
                            if (con.State == ConnectionState.Closed)
                            {
                                con.Open();
                            }
                            cmd.ExecuteNonQuery();
                            con.Close();
                            action = "INSERT";
                        }
                    }
                }
            }

            return JsonConvert.SerializeObject(new { result = true, message = action });

        }

   
        [WebMethod]
        public string SaveUserDataWLO(string USER_DATA)//WebMethod to Save the data  
        {
            var serializeData_UserData = JsonConvert.DeserializeObject<List<AddUserWLO>>(USER_DATA);

            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                foreach (var data in serializeData_UserData)
                {
                    using (SqlCommand cmd = new SqlCommand("INSERT INTO WLO_ACCOUNT VALUES (@USERNAME,'abc123',@NAME,@EMAIL,'false')", con))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@USERNAME", data.USERNAME);
                        cmd.Parameters.AddWithValue("@NAME", data.NAME);
                        cmd.Parameters.AddWithValue("@EMAIL", data.EMAIL);
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }

            }
            return null;
        }
        #endregion

        //AGENT PAGE REGION
        #region

        [WebMethod(enableSession:true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public UOM getUomByStockCode(string stockcode)//WebMethod to get the uom USING STOCKCODE  
        {
            string COMPANY_CD = HttpContext.Current.Session["company_code"].ToString();
            string qneConnection = "";

            //        System.Diagnostics.Debug.WriteLine("COMPANY_CD = " + COMPANY_CD);

            try
            {
                qneConnection = QNEConnectionString.ChooseConnection(COMPANY_CD);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                //  return;
            }
            UOM uom = new UOM();

            using (var con = new FbConnection(qneConnection))
            {
                using (FbCommand cmd = new FbCommand("SELECT STOCKCODE,UOM FROM STOCK WHERE STOCKCODE = @code", con))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@code", stockcode);
                    cmd.Connection = con;
                    con.Open();
                    using (FbDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            uom.STOCKCODE = sdr["STOCKCODE"].ToString();
                            uom.UOM_ = sdr["UOM"].ToString();

                        }
                    }
                    con.Close();
                }

            }
            return uom;
        }

       [WebMethod(enableSession:true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public LASTPURCHASE GetLastPurchase(string stockcode,string customercode)//WebMethod to get the uom USING STOCKCODE  
        {
            
            LASTPURCHASE lastpurchase = new LASTPURCHASE();

            string Query = "";
            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                Query += @"SELECT TOP 1 A.CUSTOMERCODE,A.AGENT,B.STOCKCODE,B.PRICE FROM QUOTATION AS A
                            LEFT JOIN QUOTATION_DETAIL AS B
                            ON A.QT_ID_PK = B.QUOTATION_FK
                            WHERE B.STOCKCODE = @stockcode AND A.CUSTOMERCODE = @customercode
                            ORDER BY A.QT_ID_PK DESC";
                using (SqlCommand cmd = new SqlCommand(Query, con))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@stockcode", stockcode);
                    cmd.Parameters.AddWithValue("@customercode", customercode);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            lastpurchase.CUSTOMERCODE = sdr["CUSTOMERCODE"].ToString();
                            lastpurchase.SALESAGENT = sdr["AGENT"].ToString();
                            lastpurchase.STOCKCODE = sdr["STOCKCODE"].ToString();
                            lastpurchase.PRICE = Convert.ToDouble(sdr["PRICE"].ToString());
                        }
                    }
                    con.Close();
                }

            }
            return lastpurchase;
       
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public QuotCode getLatestQuot(int company)//WebMethod to get the uom USING STOCKCODE  
        {
            //string COMPANY_CD = HttpContext.Current.Session["company_code"].ToString();
            //string qneConnection = "";

            //        System.Diagnostics.Debug.WriteLine("COMPANY_CD = " + COMPANY_CD);

            //try
            //{
            //    qneConnection = QNEConnectionString.ChooseConnection(COMPANY_CD);
            //}
            //catch (Exception ex)
            //{
            //    System.Console.WriteLine(ex.Message);
            //    //  return;
            //}
            QuotCode qt = new QuotCode();

            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT company_ID,company_Counter FROM QT_COUNTER WHERE company_ID = @code", con))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@code", company);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            qt.companyID = Convert.ToInt32(sdr["company_ID"].ToString());
                            qt.companyCounter = Convert.ToInt32(sdr["company_Counter"].ToString());

                        }
                    }
                    con.Close();
                }

            }
            return qt;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true )]
        public USERDETAIL getUserDetails(int agent_ID)//WebMethod to get the uom USING STOCKCODE  
        {
            //string agent_ID = HttpContext.Current.Session["agent_id"].ToString();

            USERDETAIL user = new USERDETAIL();

            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM account WHERE ACCOUNT_ID = @ID", con))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@ID", agent_ID);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            user.NAME = sdr["NAME"].ToString();
                            user.EMAIL = sdr["EMAIL"].ToString();
                            user.PASSWORD = sdr["PASSWORD"].ToString();

                        }
                    }
                    con.Close();
                }

            }
            return user;
        }

        [WebMethod(EnableSession = true)]
        public void DeleteQuotation(string QUOT_ID)
        {

            string UserAgent = HttpContext.Current.Session["userName"].ToString();
           
         

            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {

            

                using (SqlCommand cmd = new SqlCommand("UPDATE QUOTATION SET CANCELED = '1',CANCEL_DATE = GETDATE() WHERE QT_ID_PK = '" + QUOT_ID + "' AND AGENT = '"+UserAgent+"'", con))
                {

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    int no = cmd.ExecuteNonQuery();
                    con.Close();

                    if(no>0)
                    {
                        using (SqlCommand cmdInsertCanceled = new SqlCommand("INSERT INTO CANCELED_QT (QT_ID_FK) VALUES (@QT_ID_FK)"))
                        {
                            cmdInsertCanceled.CommandType = System.Data.CommandType.Text;
                            cmdInsertCanceled.Parameters.AddWithValue("@QT_ID_FK", QUOT_ID);
                            cmdInsertCanceled.Connection = con;

                            if (con.State == ConnectionState.Closed)
                            {
                                con.Open();
                            }
                            cmdInsertCanceled.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
            }
        }

        [WebMethod]
        public void GetQuotationDetails(string Quot_ID_MAIN)
        {
            List<QT_DETAILS> listQuot_Detail = new List<QT_DETAILS>();

            using (var con = new SqlConnection(Constr))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM QUOTATION_DETAIL WHERE QUOTATION_FK = '" + Quot_ID_MAIN + "'", con))
                {

                    cmd.CommandType = System.Data.CommandType.Text; cmd.Connection = con;

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        QT_DETAILS list = new QT_DETAILS();
                        list.STOCKCODE = dr["STOCKCODE"].ToString();
                        list.STOCKNAME = dr["STOCKNAME"].ToString();
                        list.QTY = Convert.ToDouble(dr["QTY"].ToString());
                        list.UOM = dr["UOM"].ToString();
                        list.PRICE = Convert.ToDouble(dr["PRICE"].ToString());
                        list.AMOUNT = Convert.ToDouble(dr["QTY"].ToString()) * Convert.ToDouble(dr["PRICE"].ToString()); 
                        listQuot_Detail.Add(list);
                    }
                    con.Close();

                }
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            //return js.Serialize(listQuot);
            Context.Response.Write(js.Serialize(listQuot_Detail));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public String ShowQuotations(string Agent)
        {

            try
            {
                //string qneConnection = QNEConnectionString.ChooseConnection(company_code);
                string queryString = @"SELECT * FROM QUOTATION WHERE AGENT = '"+Agent+"'";
                DataTable dt = new DataTable();

                using (SqlConnection fbConnection = new SqlConnection(Constr))
                {
                    fbConnection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(queryString, fbConnection);
                    da.Fill(dt);
                    fbConnection.Close();
                }
                return "{\"aaData\":" + DataTableToJSONWithJSONNet(dt) + "}";
            }

            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("error: " + ex.Message);
                Context.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                Context.Response.StatusDescription = ex.Message;
                return null;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public String ShowMyWlo(string Agent_ID)
        {
            try
            {
                //string qneConnection = QNEConnectionString.ChooseConnection(company_code);
                string queryString = @"SELECT A.ASISGNED_ID,C.NAME AS WLO_NAME,C.EMAIL AS WLO_EMAIL FROM ASSIGNMENT AS A
                            LEFT JOIN WLO_ACCOUNT AS C
                            ON A.WLO_ID_FK = C.ID
                            LEFT JOIN account AS D
                            on A.AGENT_ID_FK = D.ACCOUNT_ID
							WHERE A.AGENT_ID_FK = '" + Agent_ID+"'";
                DataTable dt = new DataTable();

                using (SqlConnection fbConnection = new SqlConnection(Constr))
                {
                    fbConnection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(queryString, fbConnection);
                    da.Fill(dt);
                    fbConnection.Close();
                }
                return "{\"aaData\":" + DataTableToJSONWithJSONNet(dt) + "}";
            }

            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("error: " + ex.Message);
                Context.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                Context.Response.StatusDescription = ex.Message;
                return null;
            }
        }

        [WebMethod(EnableSession = true)]
        public void DeleteAssignment(string ASS_ID)
        {
            
            using (var con = new SqlConnection(Constr))
            {
                using (SqlCommand cmd = new SqlCommand("DELETE FROM ASSIGNMENT WHERE ASISGNED_ID = '" + ASS_ID + "'", con))
                {

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();

                }
            }
        }

        [WebMethod(EnableSession = true)]
        public List<ListItem> GetWLOLISTAgent(string agent_ID)
        {

            string query = @"select * from WLO_ACCOUNT where ID NOT in (select WLO_ID_FK from ASSIGNMENT WHERE AGENT_ID_FK = '"+ agent_ID + "')";
            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    List<ListItem> depts = new List<ListItem>();
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            depts.Add(new ListItem
                            {
                                Value = sdr["ID"].ToString(),
                                Text = sdr["NAME"].ToString()
                            });
                        }
                    }
                    con.Close();
                    return depts;
                }
            }
        }

        [WebMethod]
        public String bindEditMode(string QT_ID_FK)
        {
            try
            {
                //string qneConnection = QNEConnectionString.ChooseConnection(company_code);
                string queryString = @"SELECT * FROM QUOTATION_DETAIL WHERE QUOTATION_FK = '"+QT_ID_FK+"'";
                DataTable dt = new DataTable();


                using (SqlConnection fbConnection = new SqlConnection(Constr))
                {
                    fbConnection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(queryString, fbConnection);
                    da.Fill(dt);
                    fbConnection.Close();
                }
                return DataTableToJSONWithJSONNet(dt);
            }

            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("error: " + ex.Message);
                Context.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                Context.Response.StatusDescription = ex.Message;
                return null;
            }
        }

        public string DataTableToJSONWithJSONNet(DataTable table)
        {
            string JsonString = string.Empty;
            JsonString = JsonConvert.SerializeObject(table, Formatting.None, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd hh:mm tt" });
            return JsonString;
        }

        [WebMethod(EnableSession = true)]
        public string SaveData(string QT_DETAILS, string QT_MAIN)//WebMethod to Save the data  
        {
            var serializeData_QT = JsonConvert.DeserializeObject<List<QT_DETAILS>>(QT_DETAILS);
            
            var serializeData_QT_MAIN = JsonConvert.DeserializeObject<List<QT>>(QT_MAIN);

            string QT_ID = "";
            using (var con = new SqlConnection(Constr))
            {
                foreach (var data2 in serializeData_QT_MAIN)
                {
                    using (SqlCommand cmd = new SqlCommand("SPInsertQuotation", con))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@QTCODE", data2.QTCODE);
                        cmd.Parameters.AddWithValue("@customer", data2.CUSTOMER);
                        cmd.Parameters.AddWithValue("@agent", data2.AGENT);
                        cmd.Parameters.AddWithValue("@customerName", data2.CUSTOMERNAME);
                        cmd.Parameters.AddWithValue("@agent_id", data2.AGENT_ID);
                        cmd.Parameters.AddWithValue("@company", data2.COMPANY);

                        SqlParameter output = new SqlParameter();
                        output.ParameterName = "@QT_ID";
                        output.SqlDbType = System.Data.SqlDbType.Int;
                        output.Direction = System.Data.ParameterDirection.Output;
                        cmd.Parameters.Add(output);
                        cmd.Connection = con;
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }
                        cmd.ExecuteNonQuery();
                        con.Close();
                        QT_ID = output.Value.ToString();


                    }
                }
                if(QT_ID!="")
                {

               
                foreach (var data in serializeData_QT)
                {
                    using (SqlCommand cmd = new SqlCommand("INSERT INTO QUOTATION_DETAIL VALUES (@code, @name,@qty,@uom,'"+QT_ID+ "',@price,@amount)"))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@code", data.STOCKCODE);
                        cmd.Parameters.AddWithValue("@name", data.STOCKNAME);
                        cmd.Parameters.AddWithValue("@qty", data.QTY);
                            cmd.Parameters.AddWithValue("@uom", data.UOM);
                            cmd.Parameters.AddWithValue("@price", data.PRICE);
                            cmd.Parameters.AddWithValue("@amount", data.PRICE * data.AMOUNT);
                            cmd.Connection = con;
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }

                }
            }
            return null;
        }

        [WebMethod]
        public string SaveDatEditForm(string QT_DETAILS, string QT_ID)//WebMethod to Save the data  
        {
            var serializeData_QT = JsonConvert.DeserializeObject<List<QT_DETAILS>>(QT_DETAILS);

            using (var con = new SqlConnection(Constr))
            {

                using (SqlCommand cmdInsertMod = new SqlCommand("INSERT INTO MODIFIED_QT (QT_ID_FK) VALUES (@QT_ID_FK)"))
                {
                    cmdInsertMod.CommandType = CommandType.Text;
                    cmdInsertMod.Parameters.AddWithValue("@QT_ID_FK", QT_ID);
                    cmdInsertMod.Connection = con;
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    int no = cmdInsertMod.ExecuteNonQuery();
                    con.Close();

                    if(no>0)
                    {
                        using(SqlCommand cmdUpdateQuot = new SqlCommand("UPDATE QUOTATION SET LAST_MODIFIED = GETDATE() WHERE QT_ID_PK = @QT_ID_FK"))
                        {
                            cmdUpdateQuot.CommandType = CommandType.Text;
                            cmdUpdateQuot.Parameters.AddWithValue("@QT_ID_FK", QT_ID);
                            cmdUpdateQuot.Connection = con;
                            if (con.State == ConnectionState.Closed)
                            {
                                con.Open();
                            }
                            cmdUpdateQuot.ExecuteNonQuery();
                            con.Close();

                            //InsertTrail(5, " Edit User Profile", "Update");
                        }
                    }
                }

                using (SqlCommand cmdDelete = new SqlCommand("DELETE FROM QUOTATION_DETAIL WHERE QUOTATION_FK = @QT_ID_FK"))
                {
                    cmdDelete.CommandType = CommandType.Text;
                    cmdDelete.Parameters.AddWithValue("@QT_ID_FK", QT_ID);
                    cmdDelete.Connection = con;
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    int no = cmdDelete.ExecuteNonQuery();
                    con.Close();

                    if (no > 0)
                    {

                        foreach (var data in serializeData_QT)
                        {
                            using (SqlCommand cmd = new SqlCommand("INSERT INTO QUOTATION_DETAIL VALUES (@code, @name,@qty,@uom,'" + QT_ID + "',@price,@amount)"))
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.Parameters.AddWithValue("@code", data.STOCKCODE);
                                cmd.Parameters.AddWithValue("@name", data.STOCKNAME);
                                cmd.Parameters.AddWithValue("@qty", data.QTY);
                                cmd.Parameters.AddWithValue("@uom", data.UOM);
                                cmd.Parameters.AddWithValue("@price", data.PRICE);
                                cmd.Parameters.AddWithValue("@amount", data.PRICE * data.QTY);
                                cmd.Connection = con;
                                if (con.State == ConnectionState.Closed)
                                {
                                    con.Open();
                                }
                                cmd.ExecuteNonQuery();
                                con.Close();
                            }
                        }
                    }

                }
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        public List<ListItem> GetStockCode()
        {
            string COMPANY_CD = HttpContext.Current.Session["company_code"].ToString();
            string qneConnection = "";

            //        System.Diagnostics.Debug.WriteLine("COMPANY_CD = " + COMPANY_CD);

            try
            {
                qneConnection = QNEConnectionString.ChooseConnection(COMPANY_CD);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                //  return;
            }

            string query = "SELECT STOCKCODE,STOCKNAME FROM STOCK WHERE STOCKCODE LIKE 'FG%'";

            using (FbConnection con = new FbConnection(qneConnection))
            {
                using (FbCommand cmd = new FbCommand(query))
                {
                    List<ListItem> stocks = new List<ListItem>();
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    using (FbDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            stocks.Add(new ListItem
                            {
                                Value = sdr["STOCKCODE"].ToString(),
                                Text = sdr["STOCKNAME"].ToString()
                            });
                        }
                    }
                    con.Close();
                    return stocks;
                }
            }
        }

        [WebMethod(EnableSession=true)]
        public List<ListItem> GetCustomerList()
        {
            string COMPANY_CD = HttpContext.Current.Session["company_code"].ToString();

            string qneConnection = "";

            //        System.Diagnostics.Debug.WriteLine("COMPANY_CD = " + COMPANY_CD);

            try
            {
                qneConnection = QNEConnectionString.ChooseConnection(COMPANY_CD);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                //  return;
            }

            string query = @"SELECT B.COMPANYCODE,A.DESCRIPTION FROM GLACCOUNT A
                            LEFT JOIN DEBTOR B
                            ON A.GLACCOUNTCODE = B.COMPANYCODE
                            WHERE A.ACCOUNTCODE = 'BCA' AND A.SPECIALACCOUNTCODE = 'AR'";
            using (FbConnection con = new FbConnection(qneConnection))
            {
                using (FbCommand cmd = new FbCommand(query))
                {
                    List<ListItem> customers = new List<ListItem>();
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    using (FbDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            customers.Add(new ListItem
                            {
                                Value = sdr["COMPANYCODE"].ToString(),
                                Text = sdr["DESCRIPTION"].ToString()
                            });
                        }
                    }
                    con.Close();
                    return customers;
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public void UpdateProfile(string Agent_ID,string Email,string Password)
        {

            using (var con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE account set EMAIL = '"+Email+"',PASSWORD = '"+Password+"' WHERE ACCOUNT_ID = '" + Agent_ID + "'", con))
                {

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();
                    //string name = System.Environment.MachineName;

                    InsertTrail(Agent_ID," Edit User Profile", "Update");
                }
            }
        }
        #endregion

        [WebMethod]
        public void InsertTrail(string user,string remarks,string type)
        {
            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("INSERT INTO SYSTRAIL VALUES (getdate(),@user,@remarks,@type)"))
                {

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Parameters.AddWithValue("@user", user);
                    cmd.Parameters.AddWithValue("@remarks", remarks);
                    cmd.Parameters.AddWithValue("@type", type);
                    cmd.Connection = con;

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();

                }
            }
        }






    }
}
