﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SALESAGENT_WLO.A_WLO
{
    public partial class WLOMAINPAGE : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLgOut_Click(object sender, EventArgs e)
        {

            Session.Remove("USERNAMEWLO");
            Session.Remove("ID_WLO");
            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
        }
    }
}