﻿<%@ Page Title="WLO DASHBOARD" Language="C#" MasterPageFile="~/A_WLO/WLO.Master" AutoEventWireup="true" CodeBehind="WLOMAINPAGE.aspx.cs" Inherits="SALESAGENT_WLO.A_WLO.WLOMAINPAGE" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script src="../js/WLOMAINPAGE.js"></script>
    <script>
        
        $(document).ready(function () {
            var id = "";
            var datatableVariable;

            $("#btnEditAccountMode").on("click", function (e) {
                e.preventDefault();

                var a = $(this).prop("value");
                var n = $("#name");
                var e = $("#ContentPlaceHolder1_email");
                var p = $("#password");
                var cp = $("#confirmpassword");

                if (a == "EDIT ACCOUNT") {


                    $.confirm({
                        title: 'Password Verification',
                        content: 'url:Pass.aspx',
                        buttons: {
                            MyPassword: {
                                text: 'Verify Password',
                                btnClass: 'btn-orange',
                                action: function () {

                                    var input = this.$content.find('input#input-name');
                                    var errorText = this.$content.find('.alert-danger');

                                    if (input.val().trim() == $(p).val()) {
                                        //console.log('Correct Password!');

                                        $("#btnEditAccountMode").attr("value", "SAVE ACCOUNT");

//                                        console.log("EDIT MODE!");

                                        $(e).prop('disabled', false);
                                        $(p).prop('disabled', false);

                                        $('.confirm').show("fast");
                                        $(errorText).hide("fast");
                                    } else {
                                        $(errorText).show("fast");
                                        return false;
                                    }
                                }
                            },
                            cancel: function () {
                                // do nothing.
                            }
                        }
                    });


                }
                else if (a == "SAVE ACCOUNT") {



                    if ($(e).val() == '') {
                        alert("Name is Empty.");
                    }
                    else {

                        if ($(p).val() == $(cp).val()) {

                            $.ajax({
                                url: '../WebService.asmx/UpdateProfileWlo',//Home.aspx is the page   
                                type: 'POST',
                                dataType: 'json',
                                contentType: 'application/json; charset=utf-8',
                                data: JSON.stringify({'Email': $(e).val(), 'Password': $(cp).val() }),
                                success: function () {

                                    $(e).prop('disabled', true);
                                    $(p).prop('disabled', true);

                                    $("#btnEditAccountMode").attr("value", "EDIT ACCOUNT");
                                    $('.confirm').hide("fast");


                                    $.alert({
                                        autoClose: 'ok|3000',
                                        title: 'Successfully',
                                        icon: 'fa fa-warning',
                                        content: '<div class="text-center"> <strong>Update </strong> successfully!</div>'
                                    });

                                    loadAccountSettings();

                                },
                                error: function () {
                                    alert("Error deleting data");
                                }
                            })


                            //            console.log("DONE EDIT MODE!");
                        }
                        else {
                            alert("password not match");
                        }

                    }


                }


            });


            function loadAccountSettings() {

                var n = $("#name");
                var e = $("#ContentPlaceHolder1_email");
                var p = $("#password");
                var cp = $("#confirmpassword");

                $.ajax({
                    url: '../WebService.asmx/getWLOUserDetails',
                    data: '{}',
                    dataType: 'xml',
                    success: function (data) {
                        var jqueryXml = $(data);
                        $(n).val(jqueryXml.find('NAME').text());
                        $(e).val(jqueryXml.find('EMAIL').text());
                        $(p).val(jqueryXml.find('PASSWORD').text());
                        $(cp).val(jqueryXml.find('PASSWORD').text());
                        // $(c).val(1);
                    },
                    error: function (err) {
                        alert(err);
                    }
                });

            }

            loadAccountSettings();

        });
       
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    
    <br />

                                         <div class="row">
                              <div class="col-12 col-lg-12 col-md-12">
 <div class="card shadow">
  <div class="card-header">
    <ul class="nav nav-tabs card-header-tabs">
      <li class="nav-item">
        <a class="nav-link active" id="v-pills-1st-tab" data-toggle="pill" href="#pills1" role="tab" aria-selected="true" >Quotations</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="v-pills-2nd-tab" data-toggle="pill" href="#pills2" role="tab" aria-selected="false">Settings</a>
      </li>
      <li class="nav-item">
        <asp:LinkButton runat="server" class="nav-link" ID="btnLgOut" OnClick="btnLgOut_Click"><span class="fa fa-sign-out"></span> Log Out</asp:LinkButton>
      </li>
    </ul>
  </div>
  <div class="card-body">

      <div class="tab-content">
        <div class="tab-pane fade show active" id="pills1" role="tabpanel" aria-labelledby="v-pills-1st-tab">

                                        <div class="row text-center">
                                      <div class="col-4 bg-success text-white">ENCODED</div>
                                      <div class="col-4 bg-light" style="border:1px solid #dedede">PENDING</div>
                                      <div class="col-4 bg-danger text-white">CANCELED</div>
                                  </div>

            <br />
                                    <div class="row">
                                      <div class="col-12 col-lg-6 col-md-6">
      <div class="custom-control custom-checkbox custom-control-inline">
    <input type="checkbox" class="custom-control-input" id="hide2"/>
    <label class="custom-control-label" for="hide2">HIDE ENCODED </label>
</div>
                                  <div class="custom-control custom-checkbox custom-control-inline">
    <input type="checkbox" class="custom-control-input" id="hide1"/>
    <label class="custom-control-label" for="hide1">HIDE CANCELED</label>
</div>                                
  <button id="btnRefresher" class="btn btn-info btn-sm"><span class="fa fa-refresh"></span> Refresh</button>
                                          </div>
                                
                                      <div class="col-12 col-lg-6 col-md-6">
                                      

                                            <div class="form-group form-inline">
    <span id="date-label-from" class="date-label">FROM: </span><input class="date_range_filter date" type="text" id="datepicker_from" />
    <span id="date-label-to" class="date-label">TO:</span><input class="date_range_filter date" type="text" id="datepicker_to" />
                                </div>
                                      </div>
                                  </div>

              <table id="QuotTable" class="table table-striped table-bordered table-responsive-sm" style="width:100%">

                                     <thead>
                                         <tr>
                                                   <th style="width:5%"></th>
                                             <th style="display:none"></th>
                                             <th style="width:15%">QTCODE</th>
                                             <th style="width:15%">CREATED</th>
                                             <th style="width:10%">QNE CODE</th>
                                             <th style="width:15%">QNE DATE</th>
                                             <th style="width:25%">CUSTOMER CODE/NAME</th>
                                             <th style="width:10%">AGENT</th>
                                         </tr>
                                     </thead>
                      
                                         </table>
            </div>

           <div class="tab-pane fade" id="pills2" role="tabpanel" aria-labelledby="v-pills-2nd-tab">
               <div class="card">
  <div class="card-header">Account</div>
  <div class="card-body">
                        <div role="form" id="CreateForm">
                         <div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" required="required" disabled="disabled" id="name" placeholder="Name"/>
    </div>
  </div>
  
  <div class="form-group row">
    <label for="email" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
      <input type="email" runat="server" class="form-control" disabled="disabled" id="email" placeholder="Email"/>
    </div>
  </div>
  <div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="password" placeholder="Password"/>
    </div>
  </div>
      <div class="form-group row confirm" style="display:none">
    <label for="password" class="col-sm-2 col-form-label">Confirm Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="confirmpassword" placeholder="Password"/>
    </div>
  </div>


  <div class="form-group row">
    <div class="col-sm-4 offset-sm-4">
      <input type="button" id="btnEditAccountMode" class="btn btn-primary btn-block" value="EDIT ACCOUNT"/>
    </div>
  </div>
</div>
          
      </div>
                    </div>
          </div>
</div>
  </div>
</div>
 
                                  </div> <!-- end of 12 -->
                    </div> <!-- end of row -->
   
     
    
            
</asp:Content>
