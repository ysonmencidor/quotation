﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace SALESAGENT_WLO.A_WLO
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["USERNAMEWLO"] != null && Session["ID_WLO"] != null)
                {
                    Response.Redirect("WLOMAINPAGE.aspx");
                }
            }
        }

        protected void btnloginWLO_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(SQLCONNECTION.SQLSVRConnection()))
            {
                SqlCommand cmd = new SqlCommand("select PASSWORD,ID from WLO_ACCOUNT where USERNAME = @use", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@use", txtusername.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {

                    if (dt.Rows[0][0].ToString() == txtpassword.Text)
                    {

                        Session["USERNAMEWLO"] = txtusername.Text;
                        Session["ID_WLO"] = dt.Rows[0][1].ToString();
                        Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
                    }
                    else
                    {
                        txtpassword.Focus();
                        divalert.Visible = true;
                    //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Invalid password.');", true);

                    }
                }
                else
                {
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Cannot find user account.');", true);

                    divalert.Visible = true;
                }
            }
        }
    }
}