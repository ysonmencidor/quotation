﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Newtonsoft.Json;
using System.Web.Services;
using FirebirdSql.Data.FirebirdClient;
using FirebirdSql.Data;

namespace SALESAGENT_WLO
{
    public partial class Index : System.Web.UI.Page
    {
        public static string Constr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        public static string ConstrAphi = ConfigurationManager.ConnectionStrings["QNE_APHI"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["userName"] == null && Session["company_code"] == null && Session["agent_id"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    hidden_company.Value = Session["company_code"].ToString();
                    hidden_user.Value = Session["userName"].ToString();
                    hiddent_agent.Value = Session["agent_id"].ToString();
                }
            }
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Session.Remove("userName");
            Session.Remove("company_code");
            Session.Remove("agent_id");
            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
        }
    }
    
}