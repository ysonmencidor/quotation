﻿$(document).ready(function () {

    var hidden_company = $('#hidden_company').val();
    var hidden_user = $('#hidden_user').val();
    var hidden_agent = $('#hiddent_agent').val();

    if (hidden_company == "1") {
        $("#IMG_LOGO").attr("src", "img/APHILOGO.png");
    }
    else {
        $("#IMG_LOGO").attr("src", "img/AVLILOGO.jpg");
    }
    var id = "";
    var datatableVariable;

    $("#btnEditAccountMode").on("click", function (e) {
        e.preventDefault();

        var a = $(this).prop("value");
        var n = $("#name");
        var e = $("#email");
        var p = $("#password");
        var cp = $("#confirmpassword");

        if (a == "EDIT ACCOUNT") {


            $.confirm({
                title: 'Password Verification',
                content: 'url:Pass.aspx',
                buttons: {
                    MyPassword: {
                        text: 'Verify Password',
                        btnClass: 'btn-orange',
                        action: function () {

                            var input = this.$content.find('input#input-name');
                            var errorText = this.$content.find('.alert-danger');

                            if (input.val().trim() == $(p).val()) {
                                console.log('Correct Password!');

                                $("#btnEditAccountMode").attr("value", "SAVE ACCOUNT");

                                console.log("EDIT MODE!");

                                $(e).prop('disabled', false);
                                $(p).prop('disabled', false);

                                $('.confirm').show("fast");
                                $(errorText).hide("fast");
                            } else {
                                $(errorText).show("fast");
                                return false;
                            }
                        }
                    },
                    cancel: function () {
                        // do nothing.
                    }
                }
            });


        }
        else if (a == "SAVE ACCOUNT") {



            if ($(e).val() == '') {
                alert("Name is Empty.");
            }
            else {

                if ($(p).val() == $(cp).val()) {





                    $.ajax({
                        url: 'WebService.asmx/UpdateProfile',//Home.aspx is the page   
                        type: 'POST',
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify({ 'Agent_ID': hidden_agent, 'Email': $(e).val(), 'Password': $(cp).val() }),
                        success: function () {

                            $(e).prop('disabled', true);
                            $(p).prop('disabled', true);

                            $("#btnEditAccountMode").attr("value", "EDIT ACCOUNT");
                            $('.confirm').hide("fast");


                            $.alert({
                                autoClose: 'ok|3000',
                                title: 'Successfully',
                                icon: 'fa fa-warning',
                                content: '<div class="text-center"> <strong>Update </strong> successfully!</div>'
                            });

                            loadAccountSettings();

                        },
                        error: function () {
                            alert("Error deleting data");
                        }
                    })


                    //            console.log("DONE EDIT MODE!");
                }
                else {
                    alert("password not match");
                }

            }


        }


    });


    function loadAccountSettings() {

        var n = $("#name");
        var e = $("#email");
        var p = $("#password");
        var cp = $("#confirmpassword");

        $.ajax({
            url: 'WebService.asmx/getUserDetails',
            data: { 'agent_ID': hidden_agent },
            dataType: 'xml',
            success: function (data) {
                var jqueryXml = $(data);
                $(n).val(jqueryXml.find('NAME').text());
                $(e).val(jqueryXml.find('EMAIL').text());
                $(p).val(jqueryXml.find('PASSWORD').text());
                $(cp).val(jqueryXml.find('PASSWORD').text());
                // $(c).val(1);
            },
            error: function (err) {
                alert(err);
            }
        });

    }

    loadAccountSettings();





















    //function LoadAssignment() {
    //    datatableVariable = $('#tblAssignedWLO').DataTable({
    //        stateSave: true,
    //        processing: true,    
    //        "paging":   false,
    //        "ordering": false,
    //        "info": false,
    //        "searching": false,
    //        "sAjaxSource": "WebService.asmx/ShowMyWlo",
    //        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            
    //        },
    //        "fnServerData": function (sSource, aoData, fnCallback) {
    //            $.ajax({
    //                "async": true,
    //                "dataType": 'json',
    //                "contentType": "application/json; charset=utf-8",
    //                "type": "GET",
    //                "url": sSource,
    //                "data": params = {
    //                    Agent_ID: hidden_agent
    //                },
    //                "success": function (msg) {
    //                    var json = jQuery.parseJSON(msg.d);
    //                    fnCallback(json);
    //                }
    //            });


    //        },
    //        aaSorting: [],

    //        columns: [
                   
    //                 { 'data': 'WLO_NAME' },
    //                 { 'data': 'WLO_EMAIL' },
    //                 {
    //                   "data": 'ASISGNED_ID',
    //                    "visible": false,
    //                    "searchable": false
    //                },
    //                  {
    //                      "orderable": false,
    //                      "targets": -1,
    //                      "data": null,
    //                      "defaultContent": "<button type='button' class='btn btn-primary btn-sm remover'>remove <span class='fa fa-chevron-right'></span></button>"
    //                  }
    //        ],
    //        order: [[1, 'desc']]
    //    });


        //$('#tblAssignedWLO tbody').on('click', 'button.remover[type=button]', function () {
        //    //console.log("you click the remover!!");


        //     var rowIndex = $(this).closest('tr');
        //     var data = datatableVariable.row(rowIndex).data();


        //     console.log(data.ASISGNED_ID);

        //     $.confirm({
        //         escapeKey: 'cancel',
        //         theme: 'Material',
        //         title: 'Confirmation',
        //         content: 'Are you sure you want to <strong>Remove</strong> this <br> Department : <strong class="text-danger">' + data.WLO_NAME + '</strong> ?',
        //         icon: 'fa fa-warning',
        //         animation: 'scale',
        //         closeAnimation: 'scale',
        //         opacity: 0.5,
        //         buttons: {
        //             confirm: function () {

        //                 console.log("confirmed!!");
        //                 $.ajax({
        //                     url: 'WebService.asmx/DeleteAssignment',//Home.aspx is the page   
        //                     type: 'POST',
        //                     dataType: 'json',
        //                     contentType: 'application/json; charset=utf-8',
        //                     data: JSON.stringify({ 'ASS_ID': data.ASISGNED_ID }),
        //                     success: function () {


        //                         $.alert({
        //                             autoClose: 'ok|3000',
        //                             title: 'Successfully',
        //                             icon: 'fa fa-warning',
        //                             content: '<div class="text-center"> <strong>Deleted : <span class="text-success">' + data.WLO_NAME + '</span></strong> </div>'
        //                         });
        //                         datatableVariable.ajax.reload();
        //                         loadWLOList();

        //                     },
        //                     error: function () {
        //                         alert("Error deleting data");
        //                     }
        //                 });




        //             },
        //             cancel: function () {

        //                 console.log("canceled!!!");
        //                 // $.alert('you clicked on <strong>cancel</strong>');
        //             }
        //         },

        //     });


        //});


    //}


    //$("#btnAddWLO").on("click", function (e) {

    //    e.preventDefault();

    //    var wlo = $("#ddWLOUNAssigned option:selected");

    //    //console.log(agent,department,wlo)

    //    var data = [];
    //    var U_detail = {
    //        'AGENT_ID_FK': hidden_agent,
    //        'WLO_ID_FK': wlo.val()
    //    }
    //    data.push(U_detail);

    //    var FINAL_ASSIGNMENT = JSON.stringify(data);

    //    $.ajax({
    //        url: 'WebService.asmx/SaveAssignment',//Home.aspx is the page   
    //        type: 'POST',
    //        dataType: 'json',
    //        contentType: 'application/json; charset=utf-8',
    //        data: JSON.stringify({ 'final': FINAL_ASSIGNMENT }),
    //        success: function () {
    //            alert("Assignment Successfully");
    //            datatableVariable.ajax.reload();
    //            loadWLOList();
    //        },
    //        error: function () {
    //            alert("Error while inserting data");
    //        }
    //    });
    //});


    //function loadWLOList() {
    //    $.ajax({
    //        type: "POST",
    //        url: 'WebService.asmx/GetWLOLISTAgent',
    //        data: JSON.stringify({ 'agent_ID': hidden_agent }),
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "json",
    //        success: function (r) {
    //            var ddlassignWLO = $('#ddWLOUNAssigned');
    //            ddlassignWLO.empty().append('<option selected="selected" value="0">- select wlo -</option>');
    //            $.each(r.d, function () {
    //                ddlassignWLO.append($("<option></option>").val(this['Value']).html(this['Text']));

    //            });
    //        }
    //    });
    //}

    //LoadAssignment();
    //loadWLOList();

});