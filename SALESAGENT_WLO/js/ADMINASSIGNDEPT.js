﻿var tableUser3;
$(document).ready(function () {

    loadUsers();

    function loadUsers() {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../WebService.asmx/GetDept",
            data: '{}',
            success: function (data) {
                tableUser3 = $('#tblDepartment').DataTable({
                    //  "lengthChange": false,
                    //  "processing": true,         
                    // "serverSide": false,  
                    destroy: true,
                    stateSave: true,
                    data: data,
                    columns: [
                        { 'data': 'DEPARTMENT_NAME' },
                        { 'data': 'DEPARTMENT_DESCRIPTION' }
                    ],
                    order: [[1, 'desc']]
                });
            }
        });
    }


    $("#btmSaveDept").on("click", function (e) {

        e.preventDefault();
        var username = $("#deptCode").val();
        var password = $("#deptDesc").val();

        var data = [];
        var U_detail = {
            'DEPARTMENT_NAME': username,
            'DEPARTMENT_DESCRIPTION': password
        }
        data.push(U_detail);

        var USER_DATA = JSON.stringify(data);

        $.ajax({
            url: '../WebService.asmx/SaveDepartment',//Home.aspx is the page   
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ 'DEPT_DATA': USER_DATA }),
            success: function () {
                alert("Create Department Successfully");
                console.log("SUCCESS");
                $("#deptCode").val('');
                $("#deptDesc").val('');
                $('#collapseOne2').collapse('hide');
                loadUsers();
            },
            error: function () {
                alert("Error while inserting data");
            }
        });

    });

    $(".collapse.show").each(function () {
        $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });

    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });
});