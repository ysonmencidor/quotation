﻿var tableUser;
var editMode = false;
var userId = 0;

$(function () {

    loadUsers();

    $('#CreateForm').validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            username: {
                required: true
            },
            name: {
                required: true
            }
        },
        messages: {
            email: {
                required: 'Please enter an email address.',
                email: 'Please enter a <em>valid email</em>'
            },
            username: {
                required: 'Please enter Username'
            },
            name: {
                required: 'Please enter Name'
            }
        }
    });



    $("#CreateForm").submit(function (e) {

        e.preventDefault();
        var form = $("#CreateForm");
        if (form.valid() == true) {


            var username = $("#username").val();

            if (editMode == true) {
                SaveUserData();
            }
            else {

                $.ajax({
                    url: '../WebService.asmx/CheckUserExist',//Home.aspx is the page   
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({ Username: username }),
                    success: function (response) {

                        if (response != null && response.d != null) {


                            var data = response.d;

                            data = $.parseJSON(data);


                            if (data.result == true) {
                                alert("Username already Exists");
                            }
                            else {

                                SaveUserData();
                            }


                        }
                    },
                    error: function (msg) {
                        alert(msg);
                    }
                });

            }
        }

        
    });


    collapsez();
});

function SaveUserData() {
    var company = $("input[name='gridRadios']:checked").val();
    var username = $("#username").val();
    var number = $("#number").val();
    var fullname = $("#name").val();
    var email = $("#email").val();
           var data = [];
            var U_detail = {
                'USERNAME': username,
                'CONTACTNUMBER': number,
                'COMPANY': company,
                'EMAIL': email,
                'NAME': fullname,
                'ID': userId

            }
            data.push(U_detail);

            var USER_DATA = JSON.stringify(data);

            $.ajax({
                url: '../WebService.asmx/SaveUserData',//Home.aspx is the page   
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ 'USER_DATA': USER_DATA }),
                success: function (response) {

                    if (response != null && response.d != null) {

                        var data = response.d;

                        data = $.parseJSON(data);
                        if (data.result == true) {  
                            console.log(data.message);
                        }
                    }

                     if (editMode == true) {
                         alert("Edit Account Successfully");
                         editMode = false;
                         userId = 0;
                    }
                    else {
                        alert("Create Account Successfully");
                    }
                    $("#username").val('');
                    $("#number").val('');
                    $("#name").val('');
                    $("#email").val('');
                    $('#collapseOne').collapse('hide');
                    loadUsers();
                },
                error: function () {
                    alert("Error while inserting data");
                }
            });
}

function CheckSpace(event) {
    if (event.which == 32) {
        event.preventDefault();
        return false;
    }
}

function loadUsers() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../WebService.asmx/GetUsers",
        data: '{}',
        success: function (data) {
            tableUser = $('#tblUsers').DataTable({
                //  "lengthChange": false,
                //  "processing": true,         
                // "serverSide": false,  
                destroy: true,
                stateSave: true,
                data: data,
                columns: [
                    { 'data': 'USERNAME' },
                    { 'data': 'EMAIL' },
                    { 'data': 'NAME' },
                    {
                        'data': 'COMPANY', 'render': function (company) {
                            if (company == 1) {
                                return 'APHI';
                            }
                            else {
                                return 'AVLI';
                            }

                        }
                    },
                    {
                        'data': 'ID',
                        'render': function (id) {
                            return '<button type="button" onclick=BindEditSr(' + id + ') class="btn btn-primary btn-sm editor"><span class="fa fa-edit"></span></button>' +
                                '<button type="button" onclick=DeactivateSr(' + id + ') class="btn btn-outline-secondary btn-sm deleter ml-1 "><span class="fa fa-ban"></span></button>'

                        }
                    },

                ],
                order: [[1, 'desc']],

                "createdRow": function (row, data, index) {

                    if (data.DEACTIVATED == '1') {
                        $('td', row).eq(4).html('<button type="button" onclick=ReactivateSrUser(' + data.ID + ') class="btn btn-secondary btn-sm deleter"><span class="fa fa-ban"></span></button>');
                    }

                }
            });
        }
    });
}

function BindEditSr(id) {

    var a = $('#name');
    var b = $('#number');
    var c = $('#username');
    var d = $('#email');

    $.ajax({
        url: '../WebService.asmx/GetUserById',
        data: { userid: id },
        method: 'POST',
        dataType: 'xml',
        success: function (data) {
            var jqueryXml = $(data);
            //$(a).val(jqueryXml.find('STOCKCODE').text());

            userId = jqueryXml.find('ID').text();
            $(a).val(jqueryXml.find('NAME').text());
            $(b).val(jqueryXml.find('NUMBER').text());
            $(c).val(jqueryXml.find('USERNAME').text());
            $(d).val(jqueryXml.find('EMAIL').text());

            if (jqueryXml.find('COMPANY').text() == "2") {
                $("#company02").prop("checked", true);
            }
            else {
                $("#company01").prop("checked", true);
            }
            editMode = true;
            $('#collapseOne').collapse('show');
            $("#btnSave").html('UPDATE');
        },
        error: function (err) {
            alert(err);
        }
    });


}

function ReactivateSrUser(id) {

    if (confirm('Are you sure you want to activate this user?')) {
        $.ajax({
            url: '../WebService.asmx/ReactivateSRUser',//Home.aspx is the page   
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ 'id': id }),
            success: function () {
                alert("Activate Account successfully");
                loadUsers();
            },
            error: function () {
                alert("Error while inserting data");
            }
        });
    }
}

function DeactivateSr(id) {
    if (confirm('Are you sure you want to deactivate this user?')) {

        $.ajax({
            url: '../WebService.asmx/DeactivateSRUser',//Home.aspx is the page   
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ 'id': id }),
            success: function () {
                alert("Deactivate Account successfully");
                loadUsers();
            },
            error: function () {
                alert("Error while inserting data");
            }
        });

    }
}

function collapsez() {
    $(".collapse.show").each(function () {
        $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });

    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });
}