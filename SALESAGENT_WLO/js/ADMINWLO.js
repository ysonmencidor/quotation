﻿var tableUser2;
var editMode = false;
var userId;

$(document).ready(function () {

    loadUsersWlo();
    //
  
   $("#btnSaveWLO").on("click", function (e) {

            e.preventDefault();

            if (editMode == true) {

                var username = $('#usernamewlo').val();
                var email = $('#emailwlo').val();
                var departmentname = $('#namewlo').val();

                $.ajax({
                    url: '../WebService.asmx/UpdateWloUser',//Home.aspx is the page   
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({ 'username': username, 'name': departmentname, 'email': email, 'id': userId }),
                    success: function () {

                        alert("Update Successfully");
                        $("#btnSaveWLO").html('Create Account');
                        console.log('update successfully');
                        editMode = false;
                        loadUsers();

                        $('#usernamewlo').val('');
                        $('#emailwlo').val('');
                        $('#namewlo').val('');

                        $('#collapseOne1').collapse('hide');


                    },
                    error: function () {
                        alert("Error while inserting data");
                    }
                });


            
            }
            else {

            var username = $("#usernamewlo").val();
            var fullname = $("#namewlo").val();
            var email = $("#emailwlo").val();

            var data = [];
            var U_detail = {
                'USERNAME': username,
                'EMAIL': email,
                'NAME': fullname
            }
            data.push(U_detail);

            var USER_DATA = JSON.stringify(data);

            $.ajax({
                url: '../WebService.asmx/SaveUserDataWLO',//Home.aspx is the page   
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ 'USER_DATA': USER_DATA }),
                success: function () {
                    alert("Create Account Successfully");
                    console.log("SUCCESS");
                    $("#usernamewlo").val('');
                    $("#namewlo").val('');
                    $("#emailwlo").val('');
                    $('#collapseOne1').collapse('hide');
                    loadUsersWlo();
                },
                error: function () {
                    alert("Error while inserting data");
                }
            });

            }
        });
    

    $(".collapse.show").each(function () {
        $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });

    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });
});


function loadUsersWlo() {

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../WebService.asmx/GetUsersWLO",
        data: '{}',
        success: function (data) {
            tableUser2 = $('#tblwloUsers').DataTable({
                //  "lengthChange": false,
                //  "processing": true,         
                // "serverSide": false,  
                destroy: true,
                stateSave: true,
                data: data,
                columns: [
                    { 'data': 'USERNAME' },
                    { 'data': 'EMAIL' },
                    { 'data': 'NAME' },
                    {
                        'data': 'ID',
                        'render': function (id) {
                            return '<button type="button" onclick=BindEditWlo(' + id + ') class="btn btn-primary btn-sm editor"><span class="fa fa-edit"></span></button>' +
                                '<button type="button" onclick=DeactivateWlo(' + id + ') class="btn btn-outline-secondary btn-sm deleter ml-1 "><span class="fa fa-ban"></span></button>'

                        }
                    },
                ],
                order: [[1, 'desc']],
                "createdRow": function (row, data, index) {

                    if (data.DEACTIVATED == '1') {
                        $('td', row).eq(3).html('<button type="button" onclick=ReactivateWlo(' + data.ID + ') class="btn btn-secondary btn-sm deleter"><span class="fa fa-ban"></span></button>');
                    }
                    
                }
            });
        }
    });
}

function ReactivateWlo(id) {

    if (confirm('Are you sure you want to activate this user?')) {
        $.ajax({
            url: '../WebService.asmx/ReactivateWloUser',//Home.aspx is the page   
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ 'id': id }),
            success: function () {
                alert("Activate Account successfully");
                loadUsersWlo();
            },
            error: function () {
                alert("Error while inserting data");
            }
        });
    }
}

function DeactivateWlo(id) {
    if (confirm('Are you sure you want to deactivate this user?')) {

        $.ajax({
            url: '../WebService.asmx/DeactivateWloUser',//Home.aspx is the page   
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ 'id': id }),
            success: function () {
                alert("Deactivate Account successfully");
                loadUsersWlo();
            },
            error: function () {
                alert("Error while inserting data");
            }
        });

    }
}

function BindEditWlo(id) {
    $('#collapseOne1').collapse('show');

    var a = $('#usernamewlo');
    var b = $('#emailwlo');
    var c = $('#namewlo');

    $.ajax({
        url: '../WebService.asmx/GetWLObyId',
        data: { wloId: id },
        method: 'POST',
        dataType: 'xml',
        success: function (data) {
            var jqueryXml = $(data);
            //$(a).val(jqueryXml.find('STOCKCODE').text());
            userId = jqueryXml.find('ID').text();
            $(a).val(jqueryXml.find('USERNAME').text());
            $(b).val(jqueryXml.find('EMAIL').text());
            $(c).val(jqueryXml.find('NAME').text());

            editMode = true;

            //console.log(userId);

            $("#btnSaveWLO").html('UPDATE');
            //alert(mode.val());
            //assID = jqueryXml.find('ASISGNED_ID').text();
            // $(c).val(1);
        },
        error: function (err) {
            alert(err);
        }
    });


}