﻿$(document).ready(function () {

    var table;

    $('#btnRefresher').click(function () {
        event.preventDefault();
        //table.ajax.reload();

        $('#QuotTable').DataTable().ajax.reload();
       // filterreload();
      //  table.draw();
    });

    table = $('#QuotTable').DataTable({
        stateSave: true,
        processing: true,
        "sAjaxSource": "../WebService.asmx/GetQuotationsWLo",
        "fnRowCallback": function (nRow, aData) {

            //$('td:eq(1)', nRow).attr('onclick', 'copyToClipboard("'+aData.QTCODE+'")');

            //$('td:eq(1)', nRow).attr('style', 'cursor:pointer');

            if (aData.QNE == '1') {
                $(nRow).addClass('bg-success text-white');
            }
            if (aData.CANCELED == '1') {
                $('td:eq(0)', nRow).removeClass();
                $(nRow).addClass('bg-danger text-white');
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "async": true,
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": params = {},
                "success": function (msg) {
                    var json = jQuery.parseJSON(msg.d);
                    fnCallback(json);
                },
                "error": function () {
                    console.log("DataTables warning: JSON data from server failed to load or be parsed. " +
                    "This is most likely to be caused by a JSON formatting error.");
                }
            });


        },
        columns: [
                 {
                     "className": 'details-control',
                     "orderable": false,
                     "data": null,
                     "defaultContent": ''
                 }, {
                     "data": 'QT_ID_PK',
                     "visible": false,
                     "searchable": false
                 },
                 { 'data': 'QTCODE' },
                 {
                     'data': 'DATE'
                 },
                  {
                      'data': 'QNE_QTCODE'
                  },
                   {
                       'data': 'QNE_DATE'
                   },
                 {
                     'data': { CUSTOMERNAME: "CUSTOMERNAME", CUSTOMERCODE: "CUSTOMERCODE" },
                     mRender: function (data, type, full) {
                         return '[<b> ' + data.CUSTOMERCODE + '</b> ] ' + data.CUSTOMERNAME;
                     }
                 },
                 {
                     'data': "AGENT"
                 },

        ],
        order: [[1, 'desc']],
    });


    $('#QuotTable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        //console.log("CLICKED!");

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
            });
        }
        else {

            // Open this row
            row.child(format(row.data()), 'no-padding').show();
            $('div.slider', row.child()).slideDown();
        }

    });

    function format(d) {

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../WebService.asmx/GetQuotationDetails",
            data: { 'Quot_ID_MAIN': d.QT_ID_PK },
            success: function (data) {
                var datatableVariable = $('#qtDetails' + d.QT_ID_PK + '').DataTable({
                    "lengthChange": false,
                    "searching": false,
                    "paging": false,
                    "ordering": false,
                    "bInfo": false,
                    "responsive": true,
                    data: data,
                    columns: [
                        {
                            "ordering": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        { 'data': 'STOCKCODE' },
                        { 'data': 'STOCKNAME' },
                        { 'data': 'QTY' },
                        { 'data': 'UOM' },
                        { 'data': 'PRICE' },
                        { 'data': 'AMOUNT'}
                    ],
                    "order": [[1, 'asc']],
                    "footerCallback": function (row, data, start, end, display) {
                        var api = this.api(), data;

                        // Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                i : 0;
                        };


                        // Total over this page
                        pageTotal = api
                            .column(6, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Update footer
                        $(api.column(6).footer()).html(pageTotal);
                    }
                });
            }

        });

        // `d` is the original data object for the row
        return '<div class="slider"><table class="table table-borderless table-hover css-serial" id="qtDetails' + d.QT_ID_PK + '"  style="width:100%;">' +
            '<thead class="bg-warning"><tr>' +
                '<th>#</th>' +
                '<th>STOCKCODE</th>' +
                '<th>STOCKNAME</th>' +
                '<th>QTY</th>' +
            '<th>UOM</th>' +
            '<th>PRICE</th>' +
            '<th>AMOUNT</th>' +
            '</tr></thead>' +
    '<tfoot><tr class="table-primary"><th colspan="6" style="text-align:right">Total Amount :</th><th></th></tr></tfoot>'
        '</table></div>';


    }


    $("#datepicker_from").datepicker({
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>",
        "onSelect": function (date) {
            minDateFilter = new Date(date).getTime();
            table.draw();
        }
    }).keyup(function () {
        minDateFilter = new Date(this.value).getTime();
        table.draw();
    });

    $("#datepicker_to").datepicker({
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>",
        "onSelect": function (date) {
            maxDateFilter = new Date(date).getTime();
            table.draw();
        }
    }).keyup(function () {
        maxDateFilter = new Date(this.value).getTime();
        table.draw();
    });

    minDateFilter = "";
    maxDateFilter = "";



    $.fn.dataTableExt.afnFiltering.push(
      function (oSettings, aData, iDataIndex) {
          var nTr = oSettings.aoData[iDataIndex].nTr;


          if (typeof aData.DATE == 'undefined') {
              aData.DATE = new Date(aData[3]).getTime();
          }

          if (minDateFilter && !isNaN(minDateFilter)) {
              if (aData.DATE < minDateFilter) {
                  return false;
              }
          }

          if (maxDateFilter && !isNaN(maxDateFilter)) {
              if (aData.DATE > maxDateFilter) {
                  return false;
              }
          }


          if (($(nTr).hasClass('bg-danger')) && $('#hide1').is(':checked')) {
              return false;
          }
          else if (($(nTr).hasClass('bg-success')) && $('#hide2').is(':checked')) {
              return false;
          }
          else {

              return true;
          }

      }
    );

    $('#hide1').click(function () {
        table.draw();
    });
    $('#hide2').click(function () {
        table.draw();
    });
});

function filterreload()
{
    $.fn.dataTableExt.afnFiltering.push(
    function (oSettings, aData, iDataIndex) {
        var nTr = oSettings.aoData[iDataIndex].nTr;


        if (typeof aData.DATE == 'undefined') {
            aData.DATE = new Date(aData[3]).getTime();
        }

        if (minDateFilter && !isNaN(minDateFilter)) {
            if (aData.DATE < minDateFilter) {
                return false;
            }
        }

        if (maxDateFilter && !isNaN(maxDateFilter)) {
            if (aData.DATE > maxDateFilter) {
                return false;
            }
        }


        if (($(nTr).hasClass('bg-danger')) && $('#hide1').is(':checked')) {
            return false;
        }
        else if (($(nTr).hasClass('bg-success')) && $('#hide2').is(':checked')) {
            return false;
        }
        else {

            return true;
        }

    }
  );
}
//function copyToClipboard(qt) {

//    $('#hiddenqtcode').val(qt);
//  // alert( $('#hidden').val())

//    //alert($('#ContentPlaceHolder1_hiddencode').val());

//    var copyText = document.getElementById("hiddenqtcode");
//    copyText.select();
//    document.execCommand("copy");

//    alert("Copied the text: " + copyText.value);
//}