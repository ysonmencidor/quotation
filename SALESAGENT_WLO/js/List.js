﻿
function InsertTrail(User, Remarks, Type) {
    $.ajax({
        url: 'WebService.asmx/InsertTrail',//Home.aspx is the page   
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({ 'user': User, 'remarks': Remarks, 'type': Type }),
        error: function () {
            console.log("ERROR INSERTING TRAIL");
        }
    });
}




var datatableVariable;
$(document).ready(function () {
   
    var hidden_company = $('#hidden_company').val();
    var hidden_user = $('#hidden_user').val();

    if (hidden_company == "1") {
        $("#IMG_LOGO").attr("src", "img/APHILOGO.png");
    }
    else {
        $("#IMG_LOGO").attr("src", "img/AVLILOGO.jpg");
    }
 

    function InitTable() {
        datatableVariable = $('#QuotTable').DataTable({
            stateSave: false,
            processing: true,
            "sAjaxSource": "WebService.asmx/ShowQuotations",
            "fnRowCallback": function (nRow, aData) {

                $('td:eq(1)', nRow).attr('onclick', 'copyToClipboard(this)')

                if (aData.QNE == '1')
                {
                    $('td:eq(6)', nRow).html('<img src="img/SysIcon.ico"/>')
                    $(nRow).addClass('bg-success text-white');
                }
                if (aData.CANCELED == '1')
                {
                    $('td:eq(0)', nRow).removeClass();
                    $('td:eq(6)', nRow).html('<strong>CANCELED</strong>');
                    $(nRow).addClass('bg-danger text-white');
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                    "async": true,
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": params = {
                        Agent: JSON.stringify(hidden_user)
                    },
                    "success": function (msg) {
                        var json = jQuery.parseJSON(msg.d);
                        fnCallback(json);
                    },  
                        "error": function () {  
                            console.log("DataTables warning: JSON data from server failed to load or be parsed. " +  
                            "This is most likely to be caused by a JSON formatting error.");  
                        } 
                });


            },
            columns: [
                     {
                         "className": 'details-control',
                         "orderable": false,
                         "data": null,
                         "defaultContent": ''
                     }, {
                         "data": 'QT_ID_PK',
                         "visible": false,
                         "searchable": false
                     }, 
                     { 'data': 'QTCODE' }, 
                     {
                         'data': 'DATE',
                     },
                      {
                          'data': 'QNE_QTCODE',
                      },
                       {
                           'data': 'QNE_DATE',
                       },
                     { 'data': 'CUSTOMERNAME' },
                     {
                         "orderable": false,
                         "targets": -1,
                         "data": null,
                         "defaultContent": "<button type='button' class='btn btn-primary btn-sm editor' data-toggle='modal' data-target='#exampleModal'><span class='fa fa-edit'></span></button>"+
                             " <button type='button' class='btn btn-danger btn-sm deleter'><span class='fa fa-trash'></span></button>"
                     }
            ],
            order: [[1, 'desc']],
        });

      


   
    //$.ajax({
    //    type: "POST",
    //    dataType: "json",
    //    url: "WebService.asmx/ShowQuotations",
    //    data: '{}',
    //    success: function (data) {
    //        var datatableVariable = $('#QuotTable').DataTable({
    //            //  "lengthChange": false,
    //            //  "processing": true,         
    //            // "serverSide": false,  
    //            destroy: true,
    //            stateSave: true,
    //            data: data,
    //            columns: [
    //                {
    //                    "className": 'details-control',
    //                    "orderable": false,
    //                    "data": null,
    //                    "defaultContent": ''
    //                }, {
    //                    "data": 'QT_ID',
    //                    "visible": false,
    //                    "searchable": false
    //                },
    //                { 'data': 'QTCODE' },
    //                {
    //                    'data': 'DATE_SAVE', 'render': function (date) {
    //                        var date = new Date(parseInt(date.substr(6)));
    //                        var month = date.getMonth() + 1;
    //                        return month + "-" + date.getDate() + "-" + date.getFullYear();
    //                    }
    //                },
    //                { 'data': 'CUSTOMERNAME' },
    //                {
    //                    "orderable": false,
    //                    "targets": -1,
    //                    "data": null,
    //                    "defaultContent": "<button type='button' class='btn btn-primary btn-sm editor' data-toggle='modal' data-target='#exampleModal'><span class='fa fa-edit'></span></button>"+
    //                        " <button type='button' class='btn btn-danger btn-sm deleter'><span class='fa fa-trash'></span></button>"
    //                }
    //            ],
    //            "order": [[1, 'desc']]
    //        });
    //    }
    //    });
                $('#QuotTable tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = datatableVariable.row(tr);

                    if (row.child.isShown()) {
                        // This row is already open - close it
                        $('div.slider', row.child()).slideUp(function () {
                            row.child.hide();
                        });
                    }
                    else {

                        // Open this row
                        row.child(format(row.data()), 'no-padding').show();
                        $('div.slider', row.child()).slideDown();
                    }

                });

                $('#QuotTable tbody').on('click', 'button.deleter[type=button]', function () {
                    console.log("you click the deleter!!");

                    var rowIndex = $(this).closest('tr');
                    var data2 = datatableVariable.row(rowIndex).data();

                    $.confirm({
                        escapeKey: 'cancel',
                        theme: 'Material',
                        title: 'Confirmation',
                        content: 'Are you sure you want to <strong class="text-danger">Cancel</strong> this <br> Quotation : <strong class="text-danger">' + data2.QTCODE + '</strong> ?',
                        icon: 'fa fa-warning',
                        animation: 'scale',
                        closeAnimation: 'scale',
                        opacity: 0.5,
                        buttons: {
                            confirm: function () {

                                $.ajax({
                                    url: 'WebService.asmx/DeleteQuotation',//Home.aspx is the page   
                                    type: 'POST',
                                    dataType: 'json',
                                    contentType: 'application/json; charset=utf-8',
                                    data: JSON.stringify({ 'QUOT_ID': data2.QT_ID_PK }),
                                    success: function () {

                                        //LoadData();

                                        $.alert({
                                            autoClose: 'ok|3000',
                                            title: 'Successfully',
                                            icon: 'fa fa-warning',
                                            content: '<div class="text-center"> <strong>Canceled : <span class="text-success">' + data2.QTCODE + '</span></strong> </div>'
                                        });
                                        datatableVariable.ajax.reload();

                                        //var qts = $('#lblQuot').text();
                                        InsertTrail(hidden_user, "Cancel Quotation : " + data2.QTCODE, "Cancel")
                                        //$('#QuotTable').DataTable().ajax.reload(null, false);

                                        //if ($.fn.DataTable.isDataTable("#QuotTable")) {
                                        //   $('#QuotTable').DataTable().clear().destroy();
                                        //}
                                        // datatableVariable.ajax.url('WebService.asmx/GetQuotations').load();
                                       // datatableVariable.clear.draw();
                                        //datatableVariable.destroy();
                                       // datatableVariable.ajax(JSON).reload(null,false);
                                        //$('#QuotTable').DataTable().ajax.reload();
                                        //  $('#QuotTable').DataTable().ajax.data(data);

                                    },
                                    error: function () {
                                        alert("Error deleting data");
                                    }
                                });




                            },
                            cancel: function () {
                                // $.alert('you clicked on <strong>cancel</strong>');
                            }
                        },

                    });


                });

                $('#QuotTable tbody').on('click', 'button.editor[type=button]', function () {
                    //console.log("you click the editor!!");
                    $("#tbDetails tbody").find('tr').remove();

                    var rowIndex = $(this).closest('tr');
                    var data2 = datatableVariable.row(rowIndex).data();
                    var tr = $(this).closest('tr');
                    var row = datatableVariable.row(tr);

                    //if (row.child.isShown()) {
                    //    // This row is already open - close it
                    //    $('div.slider', row.child()).slideUp(function () {
                    //        row.child.hide();
                    //        tr.removeClass('shown bg-success text-white');
                    //    });
                    //}

                    //  datatableVariable.draw(false);
                    // rowIndex.addClass('shown bg-success text-white');
                    //   var wew = data[2]
                    //  alert(data2.QT_ID);

                    $('#editID').val(data2.QT_ID_PK);
                    $('#lblName').text(data2.CUSTOMERNAME);
                    $('#lblDateCreate').text(data2.DATE);
                    $('#lblQuot').text(data2.QTCODE);

                    console.log(data2.QT_ID_PK);

                    // var counter;
                    $.ajax({
                        type: "POST",
                        url: "WebService.asmx/bindEditMode",
                        data: JSON.stringify({ 'QT_ID_FK': data2.QT_ID_PK }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            var json = JSON.parse(msg.d);
                            $.each(json, function (index, obj) {
                                // var row = '<tr><td></td><td> ' + obj.STOCKCODE + ' </td> <td> ' + obj.STOCKNAME + ' </td><td> ' + obj.QTY + ' </td><td> ' + obj.UOM + ' </td><td> ' + obj.PRICE + ' </td></tr>';


                                var row = '<tr class="data-contact-person">' +
                                      '<td class="text-center"></td>' +
                                      '<td><input type="text" name="s-name' + index + '" value="' + obj.STOCKCODE + '" class="form-control form-control-sm s-code01 hidden" /><select class="form-control form-control-sm s-name01" selected="selected" id="ddlStock' + index + '" name="s-name01' + index + '"><option Value="' + obj.STOCKCODE + '">' + obj.STOCKNAME + '</option> </select></td>' +
                                      '<td><input type="number" min="1" name="s-qty' + index + '" value="' + obj.QTY + '" class="form-control form-control-sm s-qty01" /></td>' +
                                      '<td class="text-center"><input type="number"  name="s-price' + index + '" value="' + obj.PRICE + '" class="form-control form-control-sm s-price01" /></td>' +
                                      '<td><input type="text"  name="s-uom' + index + '" value="' + obj.UOM + '" class="form-control form-control-sm s-uom01" /></td>' +
                                      '<td class="text-center"><button type="button" id="btnDelete"  data-toggle="tooltip" title="Hooray!" class="deleteRow btn btn-danger btn-sm"><span class="fa fa-remove"></span> </button></td>' +
                                      '</tr>'
                                $("#tbDetails tbody").append(row);
                                //counter++;
                                //console.log(index);
                                $.ajax({
                                    type: "POST",
                                    url: 'WebService.asmx/GetStockCode',
                                    data: '{}',
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (r) {
                                        var ddlstock = $('#ddlStock' + index + '');
                                        //  ddlStock.empty().append('<option selected="selected" value="0">Please select</option>');
                                        $.each(r.d, function () {
                                            ddlstock.append($("<option></option>").val(this['Value']).html(this['Text']));
                                        });
                                    }
                                });

                            });
                        }
                    });



                });


                function getAllEmpData() {
                    var data = [];
                    $('tr.data-contact-person').each(function () {

                        var name = $(this).find('.s-name01 option:selected').text();
                        //console.log(Select_Stockcode);//  
                        var code = $(this).find('.s-code01').val();
                        var qty = $(this).find('.s-qty01').val();
                        var price = $(this).find('.s-price01').val();
                        var uom = $(this).find('.s-uom01').val();

                        if (price == '') {
                            price = 0;
                        }

                        var alldata = {
                            'STOCKCODE': code, //FName as per Employee class name in .cs  
                            'STOCKNAME': name, //LName as per Employee class name in .cs  
                            'QTY': qty, //EmailId as per Employee class name in .cs   
                            'PRICE': price, //EmailId as per Employee class name in .cs   
                            'UOM': uom //EmailId as per Employee class name in .cs   
                        }
                        data.push(alldata);
                    });
                    // console.log(data);//  
                    return data;
                }

                $(document).on("click", ".SAVEQUOTATION", function () { //
                    var qt_id = $('#editID').val();
                    var QT_details = JSON.stringify(getAllEmpData());
                    var today = new Date();
                    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

                    //console.log(QT_details);
                    //console.log(QT_MAIN);
                    //console.log(ddCustomer);

                    $.ajax({
                        url: 'WebService.asmx/SaveDatEditForm',//Home.aspx is the page   
                        type: 'POST',
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify({ 'QT_DETAILS': QT_details, 'QT_ID': qt_id }),
                        success: function () {
                            $('#exampleModal').modal('hide');
                            // alert("Data Added Successfully");

                            $.alert({
                                autoClose: 'ok|3000',
                                title: '',
                                content: '</br><div class="text-center"> <strong class="text-success">UPDATE SUCCESSFULLY!</strong> </div>'
                            });
                            datatableVariable.ajax.reload();

                            var qts = $('#lblQuot').text();
                            InsertTrail(hidden_user, "Edit Quotation : " + qts , "Modify")
                        },
                        error: function () {
                            alert("Error while inserting data");
                        }
                    });



                });

                var rowCount = 100;;

                $(document).on("click", ".classAdd", function () { //
                    rowCount++;

                    var contactdiv = '<tr class="data-contact-person">' +
                        '<td class="text-center"></td>' +
                       '<td><input type="text"  name="s-name' + rowCount + '" class="form-control form-control-sm s-code01 hidden" /><select class="form-control form-control-sm s-name01" name="s-name01' + rowCount + '" id="ddlStock' + rowCount + '"></select></td>' +
                        '<td><input type="number" min="1" name="s-qty' + rowCount + '" class="form-control form-control-sm s-qty01" /></td>' +
                        '<td class="text-center"><input type="number"  name="s-price' + rowCount + '" class="form-control form-control-sm s-price01" /></td>' +
                        '<td><input type="text"  name="s-uom' + rowCount + '" class="form-control form-control-sm s-uom01" /></td>' +
                        '<td class="text-center"><button type="button" id="btnDelete"  data-toggle="tooltip" title="Hooray!" class="deleteRow btn btn-danger btn-sm"><span class="fa fa-remove"></span> </button></td>' +
                        '</tr>';
                    $("#tbDetails tbody").append(contactdiv); // Adding these controls to Main table class  


                    $('#ddlStock' + rowCount + '').prepend($('<option> Loading... </option>'));

                    $.ajax({
                        type: "POST",
                        url: 'WebService.asmx/GetStockCode',
                        data: '{}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            var ddlStock = $('#ddlStock' + rowCount + '');
                            ddlStock.empty().append('<option selected="selected" value="0">Please select</option>');
                            $.each(r.d, function () {
                                ddlStock.append($("<option></option>").val(this['Value']).html(this['Text']));
                            });
                        }
                    });
                });
                $(document).on("click", ".deleteRow", function () {
                    $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls  
                });
                $(document).on("change", ".s-name01", function () {
                    //$(this).closest("tr").siblings().find('.s-name01').remove(); // closest used to remove the respective 'tr' in which I have my controls

                    var STOCKCODE = $(this, ":selected");


                    var a = $(this).closest("tr").find('td .s-code01');
                    var b = $(this).closest("tr").find('td .s-uom01');
                    var c = $(this).closest("tr").find('td .s-qty01');

                    $(a).val(STOCKCODE.val());

                    $.ajax({
                        url: 'WebService.asmx/getUomByStockCode',
                        data: { stockcode: STOCKCODE.val() },
                        method: 'POST',
                        dataType: 'xml',
                        success: function (data) {
                            var jqueryXml = $(data);
                            //$(a).val(jqueryXml.find('STOCKCODE').text());
                            $(b).val(jqueryXml.find('UOM_').text());
                            // $(c).val(1);
                        },
                        error: function (err) {
                            alert(err);
                        }
                    });

                    if (STOCKCODE.val() == '0') {
                        $(c).val('');
                    }
                    else {
                        $(c).val(1);
                    }

                    //alert(wew.val());
                });
                function format(d) {

                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "WebService.asmx/GetQuotationDetails",
                        data: { 'Quot_ID_MAIN': d.QT_ID_PK },
                        success: function (data) {
                            var datatableVariable = $('#qtDetails' + d.QT_ID_PK + '').DataTable({
                                "lengthChange": false,
                                "searching": false,
                                "paging": false,
                                "ordering": false,
                                "bInfo": false,
                                "responsive": true,
                                data: data,
                                columns: [
                                    {
                                        "ordering": false,
                                        "data": null,
                                        "defaultContent": ''
                                    },
                                    { 'data': 'STOCKNAME' },
                                    { 'data': 'QTY' },
                                    { 'data': 'UOM' },
                                    { 'data': 'PRICE' },
                                    { 'data': 'AMOUNT' }
                                    //{
                                    //    'data': { PRICE: "PRICE", QTY: "QTY" }
                                    //    ,
                                    //    "render": function (data) {
                                    //        return data.PRICE * data.QTY;
                                    //    }
                                    //}
                                ],
                                "order": [[1, 'asc']],
                                "footerCallback": function (row, data, start, end, display) {
                                    var api = this.api(), data;

                                    // Remove the formatting to get integer data for summation
                                    var intVal = function (i) {
                                        return typeof i === 'string' ?
                                            i.replace(/[\$,]/g, '') * 1 :
                                            typeof i === 'number' ?
                                            i : 0;
                                    };


                                    const formatter = new Intl.NumberFormat('en-US', {
                                        style: 'currency',
                                        currency: 'PHP',
                                        minimumFractionDigits: 2
                                    })
                                    // Total over this page
                                    pageTotal = api
                                        .column(5, { page: 'current' })
                                        .data()
                                        .reduce(function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0);

                                    // Update footer
                                    $(api.column(5).footer()).html(formatter.format(pageTotal));
                                }

                            });
                        }

                    });

                    // `d` is the original data object for the row
                    return '<div class="slider"><table class="table table-borderless table-hover css-serial" id="qtDetails' + d.QT_ID_PK + '"  style="width:100%">' +
                        '<thead class="bg-warning"><tr>' +
                            '<th>#</th>' +
                            '<th>STOCKNAME</th>' +
                                '<th>QTY</th>' +
                            '<th>UOM</th>' +
                            '<th>PRICE</th>' +
                            '<th>AMOUNT</th>' +
                           // '<th>AMOUNT</th>' +
                        '</tr></thead>' +
            '<tfoot><tr class="table-primary"><th colspan="5" style="text-align:right">Total Amount :</th><th></th></tr></tfoot>'
                    '</table></div>';


                }
            }
    InitTable();
        //});


    $("#datepicker_from").datepicker({
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>",
        "onSelect": function (date) {
            minDateFilter = new Date(date).getTime();
            datatableVariable.draw();
        }
    }).keyup(function () {
        minDateFilter = new Date(this.value).getTime();
        datatableVariable.draw();
    });

    $("#datepicker_to").datepicker({
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>",
        "onSelect": function (date) {
            maxDateFilter = new Date(date).getTime();
            datatableVariable.draw();
        }
    }).keyup(function () {
        maxDateFilter = new Date(this.value).getTime();
        datatableVariable.draw();
    });

    minDateFilter = "";
    maxDateFilter = "";



    $.fn.dataTableExt.afnFiltering.push(
      function (oSettings, aData, iDataIndex) {
          var nTr = oSettings.aoData[iDataIndex].nTr;


          if (typeof aData.DATE == 'undefined') {
              aData.DATE = new Date(aData[3]).getTime();
          }

          if (minDateFilter && !isNaN(minDateFilter)) {
              if (aData.DATE < minDateFilter) {
                  return false;
              }
          }

          if (maxDateFilter && !isNaN(maxDateFilter)) {
              if (aData.DATE > maxDateFilter) {
                  return false;
              }
          }


          if (($(nTr).hasClass('bg-danger')) && $('#hide1').is(':checked')) {
              return false;
          }
          else if (($(nTr).hasClass('bg-success')) && $('#hide2').is(':checked')) {
              return false;
          }
          else {

              return true;
          }

      }
    );

    $('#hide1').click(function () {
        datatableVariable.draw();
    });
    $('#hide2').click(function () {
        datatableVariable.draw();
    });

    

});

function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}