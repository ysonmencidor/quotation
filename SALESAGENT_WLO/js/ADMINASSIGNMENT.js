﻿var datatableVariable;
var mode = "";
$(document).ready(function () {

    $(".nav-link").click(function () {
        if ($(this)[0].id == 'v-pills-assign-tab') {
            $(".pillsPrivateClass").each(function () {
                if ($(this).hasClass('active')) { /* */ } else {
                    $(this).addClass('active');
                    $(this).addClass('show');
                }
            });
            $(".pillsPublicClass").each(function () {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $(this).removeClass('show');
                }
            });
        } else {
            $(".pillsPrivateClass").each(function () {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $(this).removeClass('show');
                }
            });
        }
    });
  
    $("#btnAssign").on("click", function () {

        event.preventDefault();

        if (mode != "") {
            //alert('edit mode!' + mode.val());

            var agent = $("#assignAGENT option:selected");
            var wlo = $("#assignWLO option:selected");

            $.ajax({
                url: '../WebService.asmx/UpdateAssignment',//Home.aspx is the page   
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ 'ass_ID': mode, 'agent_FK': agent.val(), 'wlo_FK': wlo.val() }),
                success: function () {
                    alert("Update Assignment Successfully");

                    $("#btnAssign").html('ASSIGNED');
                    mode = "";

                    $('#assignAGENT').prop('selectedIndex', 0);
                    $('#assignWLO').prop('selectedIndex', 0);
                    //console.log("SUCCESS");
                    LoadAssignment();


                },
                error: function () {
                    alert("Error while inserting data");
                }
            });

        }
        else {
            var agent = $("#assignAGENT option:selected");
            var wlo = $("#assignWLO option:selected");

            //console.log(agent,department,wlo)

            var data = [];
            var U_detail = {
                'AGENT_ID_FK': agent.val(),
                'WLO_ID_FK': wlo.val()
            }
            data.push(U_detail);

            var FINAL_ASSIGNMENT = JSON.stringify(data);

            $.ajax({
                url: '../WebService.asmx/SaveAssignment',//Home.aspx is the page   
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ 'final': FINAL_ASSIGNMENT }),
                success: function () {
                    alert("Assignment Successfully");
                    //console.log("SUCCESS");
                    LoadAssignment();
                    $('#assignAGENT').prop('selectedIndex', 0);
                    $('#assignWLO').prop('selectedIndex', 0);
                    //  $("#deptCode").val('');
                    // $("#deptDesc").val('');
                    // $('#collapseOne2').collapse('hide');

                },
                error: function () {
                    alert("Error while inserting data");
                }
            });
        }

    });

    loadAgentList();
    loadWLOList();
    LoadAssignment();


});


function loadAgentList() {
    $.ajax({
        type: "POST",
        url: '../WebService.asmx/GetAgentListAdmin',
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlAgent = $('#assignAGENT');
            ddlAgent.empty().append('<option selected="selected" value="0">SELECT AGENT</option>');
            $.each(r.d, function () {
                ddlAgent.append($("<option></option>").val(this['Value']).html(this['Text']));

            });
        }
    });
}
function loadWLOList() {
    $.ajax({
        type: "POST",
        url: '../WebService.asmx/GetWloListAdmin',
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlassignWLO = $('#assignWLO');
            ddlassignWLO.empty().append('<option selected="selected" value="0">SELECT WLO</option>');
            $.each(r.d, function () {
                ddlassignWLO.append($("<option></option>").val(this['Value']).html(this['Text']));

            });
        }
    });
}
function LoadAssignment() {

    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../WebService.asmx/GetAssigned",
        data: { ass_id: 0 },
        success: function (data) {
            datatableVariable = $('#tblAssigned').DataTable({
                //  "lengthChange": false,
                //  "processing": true,         
                // "serverSide": false,  
                destroy: true,
                stateSave: true,
                data: data,
                columns: [
                    {
                        'data': 'COMPANY', 'render': function (company) {
                            if (company == 1) {
                                return 'APHI';
                            }
                            else {
                                return 'AVLI';
                            }

                        }
                    },
                    { 'data': 'AGENT_NAME' },
                    { 'data': 'AGENT_EMAIL' },
                    { 'data': 'WLO_NAME' },
                    { 'data': 'WLO_EMAIL' },
                    {
                        'data': 'ASISGNED_ID', 'render': function (ass_ID) {
                            return '<button type="button" onclick=BindEditAssignment(' + ass_ID + ') class="btn btn-primary btn-sm editor"><span class="fa fa-edit"></span></button>' +
                                '<button type="button" onclick=DeleteAssignment(' + ass_ID + ') class="btn btn-danger btn-sm deleter"><span class="fa fa-trash"></span></button>'

                        }
                    },
                ],
            });

        }
    });
}
function DeleteAssignment(ass_ID) {
    if (confirm('Are you sure you want to delete this assignment?')) {
        // Save it!

        $.ajax({
            url: '../WebService.asmx/DeleteAssignmentAdmin',//Home.aspx is the page   
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ 'ass_ID': ass_ID }),
            success: function () {
                alert("Delete assignment Successfully");
                //console.log("SUCCESS");
                LoadAssignment();
                //  $('#assignAGENT').prop('selectedIndex', 0);
                // $('#assignWLO').prop('selectedIndex', 0);
                //  $("#deptCode").val('');
                // $("#deptDesc").val('');
                // $('#collapseOne2').collapse('hide');

            },
            error: function () {
                alert("Error while inserting data");
            }
        });
    }
}
function BindEditAssignment(ass_ID) {

    //var a =

    //assignAGENT
    //assignWLO
    var a = $('#assignAGENT');
    var b = $('#assignWLO');
    mode = ass_ID;

    $.ajax({
        url: '../WebService.asmx/getAssignmentByassID',
        data: { assID: ass_ID },
        method: 'POST',
        dataType: 'xml',
        success: function (data) {
            var jqueryXml = $(data);
            //$(a).val(jqueryXml.find('STOCKCODE').text());
            $(a).val(jqueryXml.find('AGENT_ID_FK').text());
            $(b).val(jqueryXml.find('WLO_ID_FK').text());

            $("#btnAssign").html('UPDATE');
            //alert(mode.val());
            //assID = jqueryXml.find('ASISGNED_ID').text();
            // $(c).val(1);
        },
        error: function (err) {
            alert(err);
        }
    });



}