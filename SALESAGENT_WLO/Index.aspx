﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="SALESAGENT_WLO.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> -Home - </title>
     <link href="img/ntbi-icon.png" rel="shortcut icon" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-grid.min.css" rel="stylesheet" />
    <link href="Content/Style.css" rel="stylesheet" />
    <link href="Content/font-awesome.min.css" rel="stylesheet" /> 
    <script src="Scripts/jquery-3.4.1.min.js"></script>    
    <link href="Content/jquery-confirm.css" rel="stylesheet" />
    <script src="Scripts/jquery-confirm.js"></script>
    <script src="js/Index.js"></script>
    <link href="Content/Style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
  
          <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods = "true">
    </asp:ScriptManager>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10 offset-md-1 col-12">

                        <div class="x_panel shadow" style="margin-top:5px">
               <div class="x_title">

                             <div class="row">
                      <div class="col-md-6 offset-md-3 col-10 offset-1">
                     <img id="IMG_LOGO" class="img-fluid" style="margin:auto;"/>
                         </div>
                 </div>
                    

                </div>
                <div class="x_content minVH">
                                         <div class="row">

      <div class="col-12">
        
<ul style="list-style:none" class="stepNav threeWide">
  <li class="selected"><a href="Index.aspx"><i class="fa fa-dashboard"> QUOTATION</i></a></li>
    <li><a href="List.aspx"><i class="fa fa-list-alt"> LIST</i></a></li>
    <li><a href="Settings.aspx"><i class="fa fa-gear"> Settings</i></a></li>
    <li><asp:LinkButton runat="server" ID="btnLogOut" OnClick="btnLogOut_Click"><i class="fa fa-sign-out"> Log out</i></asp:LinkButton></li>
</ul>
          </div>
                                         </div>    
                      <div class="x_panel shadow">
                <div class="row">
                    <div class="col-12 col-lg-12 col-md-12">
 
                <div role="form" class="form-inline">
               </div>
                        </div>
                    </div>
                          <div role="form" >
                 <div class="form-row"> 
                     <div class="col-md-4 col-lg-4 col-6 mb-2">
                 <label for="ddlCustomer">Customer  </label>
                <select class="form-control" id="ddlCustomer"></select>
    </div> 
     <div class="col-md-3 col-lg-3 col-6 mb-2">
      <label for="latestQt">Quotation</label>
      <input readonly="true" type="text" class="form-control" id="latestQt"/>
    </div>
          <div class="col-md-3 col-lg-3 col-6 mb-2">
      <label for="validationDefault02">Date</label>
      <input readonly="true" type="text" class="form-control" id="dateNow" />
    </div>
        <div class="col-md-2 col-lg-2 col-6 mb-2">
      <label for="validationDefault02">Agent</label>
      <input readonly="true" type="text" class="form-control" id="agentview" />
    </div>
                          </div>
                              </div>
           
                          <div class="row">
                              <div class="col-12 col-lg-12 col-md-12">


                                 <table class="table table-sm table-bordered css-serial" id="wew" style="width:100%">

                                     <thead class="thead-dark text-center">
                                         <tr>
                                             <th style="width:1%">#</th>
                                             <th style="width:64%">STOCKNAME</th>
                                             <th style="width:10%">QTY</th>
                                             <th style="width:15%">U.PRICE</th>
                                             <th style="width:7%">UOM</th>
                                             <th style="width:3%"></th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                              <tr class="data-contact-person">     
                                                  <td class="text-center"></td>
                                <td><input type="text"  name="s-name" class="form-control form-control-sm s-code01 hidden" /><select class="form-control form-control-sm s-name01" id="ddlStock"></select></td>
                 <td><input type="number"  min="1"  name="s-qty" class="form-control form-control-sm s-qty01" /></td>
                 <td class="text-center"><input type="number" name="s-price" class="form-control form-control-sm s-price01" /></td>
                <td><input type="text"  name="s-uom" class="form-control form-control-sm s-uom01" /></td>
                                                  <td></td>
                                                         </tr>
                                     </tbody>
                                 </table>

                                         <button type="button" id="btnAdd" class="btn btn-sm btn-primary classAdd"><span class="fa fa-plus-square"> new row</span></button>  
                                         <button type="button" id="btnSubmit" class="btn btn-primary pull-right btn-sm SAVEQUOTATION"><span class="fa fa-save"> Save Quotation</span> </button>  
                              </div>
                          </div>          



                          </div>
         
                    <!-- dito -->

                    </div>
                </div>
            </div>
                 </div>
          </div>
        
          <input id="hidden_company" type="hidden" runat="server" />
          <input id="hidden_user" type="hidden" runat="server" />
          <input id="hiddent_agent" type="hidden" runat="server" />
    </form>
</body>
<script src="Scripts/bootstrap.min.js"></script>
</html>
