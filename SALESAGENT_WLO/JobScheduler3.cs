﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;

namespace SALESAGENT_WLO
{
    public class JobScheduler3
    {
        public static void Start()
        {
            //Email Reminder
            IJobDetail emailjob = JobBuilder.Create<QTMODIFYNOTIFTOWLO>()
                .WithIdentity("job4")
                .Build();
            ITrigger trigger = TriggerBuilder.Create()
                .StartNow()
                .WithSimpleSchedule
                (s =>
                    s.WithIntervalInMinutes(1)
                    .RepeatForever()
                )
                .ForJob(emailjob)
                .Build();

            ISchedulerFactory sf = new StdSchedulerFactory();
            IScheduler sc = sf.GetScheduler();
            sc.ScheduleJob(emailjob, trigger);
            sc.Start();
        }
    }
}