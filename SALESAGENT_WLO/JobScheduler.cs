﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;

namespace SALESAGENT_WLO
{
    public class JobScheduler
    {
        public static void Start()
        {
            //Email Reminder
            IJobDetail emailjob = JobBuilder.Create<QTCREATEDNOTIFTOWLO>()
                .WithIdentity("job2")
                .Build();
            ITrigger trigger = TriggerBuilder.Create()
                .StartNow()
                .WithSimpleSchedule
                (s =>
                    s.WithIntervalInMinutes(1)
                    .RepeatForever()
                )
                .ForJob(emailjob)
                .Build();

            ISchedulerFactory sf = new StdSchedulerFactory();
            IScheduler sc = sf.GetScheduler();
            sc.ScheduleJob(emailjob, trigger);
            sc.Start();


            IJobDetail emailjob2 = JobBuilder.Create<QTCANCELNOTIFTOWLO>()
         .WithIdentity("job3")
         .Build();
            ITrigger trigger2 = TriggerBuilder.Create()
                .StartNow()
                .WithSimpleSchedule
                (s =>
                    s.WithIntervalInMinutes(1)
                    .RepeatForever()
                )
                .ForJob(emailjob2)
                .Build();

            ISchedulerFactory sf2 = new StdSchedulerFactory();
            IScheduler sc2 = sf2.GetScheduler();
            sc2.ScheduleJob(emailjob2, trigger2);
            sc2.Start();



            IJobDetail emailjob3 = JobBuilder.Create<QTMODIFYNOTIFTOWLO>()
           .WithIdentity("job4")
           .Build();
            ITrigger trigger3 = TriggerBuilder.Create()
                .StartNow()
                .WithSimpleSchedule
                (s =>
                    s.WithIntervalInMinutes(1)
                    .RepeatForever()
                )
                .ForJob(emailjob3)
                .Build();

            ISchedulerFactory sf3 = new StdSchedulerFactory();
            IScheduler sc3 = sf3.GetScheduler();
            sc3.ScheduleJob(emailjob3, trigger3);
            sc3.Start();



            IJobDetail emailjob4 = JobBuilder.Create<CHECKENCODEAPHI>()
           .WithIdentity("job5")
           .Build();
            ITrigger trigger4 = TriggerBuilder.Create()
                .StartNow()
                .WithSimpleSchedule
                (s =>
                    s.WithIntervalInMinutes(1)
                    .RepeatForever()
                )
                .ForJob(emailjob4)
                .Build();

            ISchedulerFactory sf4 = new StdSchedulerFactory();
            IScheduler sc4 = sf4.GetScheduler();
            sc4.ScheduleJob(emailjob4, trigger4);
            sc4.Start();


            IJobDetail emailjob5 = JobBuilder.Create<CHECKENCODEAVLI>()
           .WithIdentity("job6")
           .Build();
            ITrigger trigger5 = TriggerBuilder.Create()
                .StartNow()
                .WithSimpleSchedule
                (s =>
                    s.WithIntervalInMinutes(1)
                    .RepeatForever()
                )
                .ForJob(emailjob5)
                .Build();

            ISchedulerFactory sf5 = new StdSchedulerFactory();
            IScheduler sc5 = sf5.GetScheduler();
            sc5.ScheduleJob(emailjob5, trigger5);
            sc5.Start();

        }
    }
}